import codecs
import sys
import os
import re

from as3.fileops import (IndentWriter, detect_encoding,
                         ensureConditionalImports, ensureImportsExist,
                         getIndentChars, writeReindentedViaIndentWriter)
from buildtools import log, os_utils
from unfuckEncoding import unfuckFile
from tqdm import tqdm

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
REG_PERK_OLDCODE_BLOCK_START = re.compile(re.escape('/* PERK REWORK - OLD CODE (checkPerk.py):'))
REG_PERK_REWORK_BLOCK_START = re.compile(re.escape('PERK REWORK - NEW CODE (checkPerk.py):*/'))
REG_PERK_REWORK_BLOCK_END = re.compile(re.escape('// END PERK REWORK (checkPerk.py)'))

DEBUG=True

TODO=[]
TODO_ITER=None
TODO_COMPLETED=0
TODO_LEN=0

def scan(rootdir):
    global TODO
    for root, _, files in os.walk(rootdir):
        for filename in files:
            absfilename = os.path.abspath(os.path.join(root, filename))
            relfilename = os.path.relpath(absfilename, SCRIPT_DIR)
            basefilename = os.path.basename(absfilename)
            _, ext = os.path.splitext(basefilename)
            if ext == '.as':
                TODO+=[absfilename]
            if ext in ('.bak', '.pyc'):
                log.info('Removing %s...', relfilename)
                os.remove(absfilename)

def execute():
    global TODO, TODO_LEN, TODO_COMPLETED, TODO_ITER
    TODO_LEN=len(TODO)
    TODO_COMPLETED=0
    TODO_ITER=TODO
    if not DEBUG:
        TODO_ITER = tqdm(TODO_ITER, unit='files', desc='Cleaning')
    for absfilename in TODO_ITER:
        if not DEBUG:
            TODO_ITER.desc = os.path.basename(absfilename)
        unfuckFile(absfilename, False)
        cleanFile(absfilename)

def dbgPrint(*args):
    global TODO_COMPLETED
    if DEBUG:
        print('[{}/{}] '.format(TODO_COMPLETED, TODO_LEN), *args)
def cleanFile(filename):
    global TODO_COMPLETED
    def matches(line, oline, regex, action=None):
        m = regex.search(line)
        if m is not None:
            if action != None:
                action(filename, ln, line, oline, m)
            return True
        return False

    def all_matches(line, oline, regex, action):
        for m in regex.finditer(line):
            if action != None:
                action(filename, ln, line, oline, m)

    changes = 0
    currentFileName = filename
    currentLine=''
    pathchunks = os.path.normpath(filename).split(os.sep)
    for i in range(len(pathchunks)):
        if pathchunks[i].lower().endswith('-fixed'):
            TODO_COMPLETED += 1
            return
        if pathchunks[i].lower() in ('includes', 'classes'):
            pathchunks[i] = pathchunks[i] + '-fixed'
    fixedfile = os.sep.join(pathchunks)
    fixeddir = os.path.dirname(fixedfile)
    if not os.path.isdir(fixeddir):
        os.makedirs(fixeddir)
    try:
        encoding = detect_encoding(filename)
        if encoding == 'utf-8':
            encoding = 'utf-8-sig'
        with codecs.open(filename, 'r', encoding=encoding) as f:
            with codecs.open(fixedfile, 'w', encoding='utf-8') as w:
                ln = 0
                inReworkBlock = False
                inComment = False
                inOldCodeBlock = False
                inPerkDataHeader = False
                DEBUG_WRITE = False
                for line in f:
                    ln += 1
                    currentLineNumber = ln
                    oline = line
                    lastLine = currentLine
                    currentLine = line.lstrip().strip('\r\n')
                    indent = getIndentChars(oline)
                    line = line.strip()

                    '''
                    if matches(line, oline, REG_PERK_COMMENT_HEADER, _parsedPerkDataHeader):
                        inPerkDataHeader = True
                        w.write(indent + currentLine + '\n')
                        continue
                    if inPerkDataHeader and matches(line, oline, REG_PERK_VDATA_INFO, _parsedPerkStorageName):
                        w.write(indent + currentLine + '\n')
                        continue
                    '''
                    currentPerkData = None
                    inPerkDataHeader = False

                    if matches(line, oline, REG_PERK_REWORK_BLOCK_END):
                        inReworkBlock = False
                        inOldCodeBlock = False
                        DEBUG_WRITE=True
                        dbgPrint('REG_PERK_REWORK_BLOCK_END')
                        changes += 1
                        continue
                    if matches(line, oline, REG_PERK_OLDCODE_BLOCK_START):
                        inOldCodeBlock = True
                        inReworkBlock = False
                        dbgPrint('REG_PERK_OLDCODE_BLOCK_START')
                        changes += 1
                        continue
                    if matches(line, oline, REG_PERK_REWORK_BLOCK_START):
                        if not inOldCodeBlock:
                            log.warn('REG_PERK_OLDCODE_BLOCK_START not present, continuing.')
                        inReworkBlock = True
                        inOldCodeBlock = False
                        dbgPrint('REG_PERK_REWORK_BLOCK_START')
                        changes += 1
                        continue
                    if inReworkBlock:
                        dbgPrint('REWORK: '+currentLine)
                        changes += 1
                        continue

                    lastChanges = changes
                    if line.startswith('/*'):
                        inComment = True
                    if '*/' in line:
                        inComment = False
                    w.write('{}{}\n'.format(indent, currentLine))
        #ensureImportsExist(fixedfile, ['classes.GameData.PerkRegistry'])
        if 'devTools' in fixedfile:
            dbgPrint(fixedfile)
            sys.exit(1)
        if changes == 0 and not DEBUG_WRITE:
            os.remove(fixedfile)
        else:
            log.info('%d changes written to %s', changes, fixedfile)
            #unfuckFile(fixedfile, False)
    except UnicodeError as e:
        log.exception(e)
        log.info('Skipping %s...', filename)
    TODO_COMPLETED += 1


def main():
    log.info('Cleaning classes-fixed...')
    os_utils.safe_rmtree('classes-fixed')
    log.info('Cleaning includes-fixed...')
    os_utils.safe_rmtree('includes-fixed')
    log.info('Scanning...')
    scan('classes')
    scan('includes')
    log.info('Fixing...')
    execute()
    os_utils.del_empty_dirs('classes-fixed')
    os_utils.del_empty_dirs('includes-fixed')

if __name__ == '__main__':
    main()
