#comment MUST USE 4 SPACES
#startblock if self.GenerateCheckBoxListItems:
#if WPF
public class {T}ListBox : TiTSEdit.WPF.FlagListBox<{T}Utils.CheckBoxListItem_{T}> {
    public new void InitializeComponent()
    {
        this.LoadViewFromUri("/TiTSEdit.WPF;component/controls/baseflaglistbox.xaml");
    }
}
#endif // WPF
#endblock
