import os
import re
import codecs
import shutil,yaml
import string
from datetime import date

from as3.fileops import (IndentWriter, ensureConditionalImports,
                         ensureImportsExist, getIndentChars,
                         writeReindentedViaIndentWriter, detect_encoding)
from buildtools import log
from unfuckEncoding import unfuckFile

PERKBASEDIR = 'classes/GameData/PerkClasses'
STATUSBASEDIR = 'classes/GameData/StatusEffects'

PERKREGISTRY_PACKAGE = 'classes.GameData'

KNOWN_PACKAGES = {
    r'kGAMECLASS\.': ['classes.kGAMECLASS'],
    r':PlayerCharacter': ['classes.Characters.PlayerCharacter'],
    r'ParseText\(': ['classes.Engine.Interfaces.ParseText'],
    r'rand\(': ['classes.Engine.Utility.rand'],
    r'StorageClass': ['classes.StorageClass'],
    r'GLOBAL': ['classes.GLOBAL'],
    r'NumberValueHolder': ['classes.Util.NumberValueHolder'],
    r'BoolValueHolder': ['classes.Util.BoolValueHolder'],
	r'AddLogEvent': ['classes.Engine.Interfaces.AddLogEvent'],
    r'ExtendLogEvent': ['classes.Engine.Interfaces.ExtendLogEvent'],
}


def main():
    pf = PerkFinder()
    pf.ExcludePathElement('PerkClasses')
    pf.ExcludePathElement('StatusEffects')
    pf.FindFilesIn('classes')
    pf.FindFilesIn('includes')
    pf.Process()
    for perk in pf.perks.values():
        perk.Save()
    for status in pf.statusEffects.values():
        status.Save()
    for perkID in sorted(pf.perks.keys()):
        log.info('%s (%r)', perkID, pf.perks[perkID].perkNames)
    for statusID in sorted(pf.statusEffects.keys()):
        log.info('%s (%r)', statusID, pf.statusEffects[statusID].statusNames)
    pf.DumpPerksTo('parsed_perks.yml')
    pf.DumpStatusEffectsTo('parsed_status_effects.yml')

    with open('MANUALLY_MERGE.md', 'w') as f:
        w=IndentWriter(f)
        w.writeline('# Manual Merge Checklist')
        w.writeline('These files must be manually merged.')
        w.writeline('')
        filenames={}
        for perkID in sorted(pf.perks.keys()):
            perk = pf.perks[perkID]
            for method in perk.methods:
                method.PerkID=perkID
                if method.Filename not in filenames:
                    filenames[method.Filename]=[]
                filenames[method.Filename] += [method]
        for filename, methods in filenames.items():
            if filename == '':
                continue
            with w.writeline(f'* [ ] {filename}:'):
                lines = []
                for method in methods:
                    lines+=[(method.LineNumber, f'* [ ] {method.LineNumber} - {method}')]
                for ln, line in sorted(lines, key=lambda x: x[0]):
                    w.writeline(line)


class PerkMethodParameter:

    def __init__(self, paramstr=''):
        self.Name = ''
        self.Type = ''
        self.Default = None
        self.Held = False

        if paramstr != '':
            if '=' in paramstr:
                paramstr, default = paramstr.split('=')
                paramstr = paramstr.strip()
                self.Default = default.strip()
            paramName, paramType = paramstr.split(':', 1)
            self.Name = paramName.strip()
            self.Type = paramType.strip()

    def __str__(self):
        o = '{0}:{1}'.format(self.Name, self.Type)
        if self.Default is not None:
            o += ' = {}'.format(self.Default)
        return o

    def serialize(self):
        return {
            'name': self.Name,
            'type': self.Type,
            'default': self.Default,
            'held': self.Held
        }


class PerkMethod:
    SPECIAL_METHODS = [
        'Activate',
        'Deactivate',
        'Attach',
        'Detach',
    ]

    def __init__(self):
        self.Name = ''
        self.PerkID = ''
        self.Filename = ''
        self.LineNumber = 0
        self.Parameters = []
        self.ParametersByName = {}
        self.ReturnType='void'
        self.Replacements = []
        self.CodeReplacements = []
        self.Lines = []
        self.AppendedLines=[]

    @property
    def IsSpecial(self):
        return self.Name in PerkMethod.SPECIAL_METHODS

    def RunReplacements(self, line, lineno):
        if lineno in self.AppendedLines:
            return line
        lineChunks=[]
        buffer=''
        inStr=False
        isEscaped=False

        for orig, replace, opts in self.Replacements:
            #w.writeline('// {!r} -> {!r}'.format(orig,replace))
            if 'r' not in opts:
                orig = re.escape(orig)
            try:
                line = re.sub(orig, replace, line)
            except re.error as e:
                log.error("%s: %s (when parsing %r)", e.msg, e.pattern, line)
                sys.exit(1)

        for c in line:
            if not inStr:
                if c == '"':
                    inStr = True
                    lineChunks += [(True, buffer)]
                    buffer=c
                    continue
            else:
                if isEscaped:
                    isEscaped=False
                    buffer += c
                    continue
                if c == '\\':
                    isEscaped = True
                    buffer += c
                    continue
                if c == '"':
                    buffer += c
                    lineChunks += [(False, buffer)]
                    buffer=''
                    inStr = False
                    continue
            buffer += c
        lineChunks += [(True, buffer)]
        buffer=''

        output=''
        for isCode, chunk in lineChunks:
            #print(repr((isCode, chunk)))
            if isCode:
                for orig, replace, opts in self.CodeReplacements:
                    #w.writeline('// {!r} -> {!r}'.format(orig,replace))
                    if 'r' not in opts:
                        orig = re.escape(orig)
                    try:
                        chunk = re.sub(orig, replace, chunk)
                    except re.error as e:
                        log.error("%s: %s (when parsing %r)", e.msg, e.pattern, chunk)
                        sys.exit(1)
            #output += f'/* {isCode} +{len(chunk)} */'+chunk
            output += chunk
        return output

    def serialize(self):
        return {
            'Name': self.Name,
            'PerkID': self.PerkID,
            'Filename': self.Filename,
            'LineNumber': self.LineNumber,
            'Parameters': [x.serialize() for x in self.Parameters],
            'ReturnType': self.ReturnType,
            'Replacements': self.Replacements,
            'CodeReplacements': self.CodeReplacements,
            'Lines': self.Lines,
        }

    def __str__(self):
        args=', '.join([str(arg) for arg in self.Parameters])
        return f'`@BEGINPERKFUNC {self.PerkID}.{self.Name}({args}):{self.ReturnType}`'


def makeConstName(name):
    newName = ''
    for c in name:
        c = c.upper()
        if c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ':
            newName += c
    return newName.replace(' ', '_')

class StorageClass(object):
    def __init__(self):
        self.ID = ''
        self.values = [0, 0, 0, 0]
        self.invokes = []
        self.checks = []
        self.methods = []
        self.methodsByName = {}
        self.imports = [
            'classes.GameData.PerkData',
            'classes.Creature',
            #'classes.Engine.Interfaces.AddLogEvent',
            #'classes.Engine.Interfaces.ExtendLogEvent',
            #'classes.GLOBAL',
            #'classes.StorageClass',
            #'classes.Util.NumberValueHolder',
            #'classes.Util.BoolValueHolder',
        ]
        self.placeholder = False
        self.storageInfo = {}

    def serialize(self):
        return {
            'id': self.ID,
            'values': self.values,
            'invokes': self.invokes,
            'checks': self.checks,
            'methods': [x.serialize() for x in self.methods],
            'imports': self.imports,
            'placeholder': self.placeholder,
            'storageInfo': self.storageInfo,
            'className': self.GetSafeFilename()
        }

    def GetSafeFilename(self):
        charsSinceLastBreak = 0
        o = ''
        firstchar=True
        SAFECHARS='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        for c in self.ID:
            if c.upper() in SAFECHARS:
                if charsSinceLastBreak == 0:
                    o += c.upper()
                else:
                    o += c.lower()
                charsSinceLastBreak += 1
                if firstchar:
                    SAFECHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
                    firstchar=False
            else:
                charsSinceLastBreak = 0
        return o

    def writeMethod(self, w, method):
        lines = []
        for i, line in enumerate(method.Lines):
            lines.append(method.RunReplacements(line, i))
        writeReindentedViaIndentWriter(w, lines, offset=w.indent_level)

    def getOrCreateMethod(self, name, args=[], returnType='void'):
        if name not in self.methodsByName:
            method = PerkMethod()
            method.Name = name
            for x in args:
                xp = PerkMethodParameter(x)
                method.Parameters.append(xp)
                method.ParametersByName[xp.Name] = xp
            method.ReturnType=returnType
            self.methods.append(method)
            self.methodsByName[method.Name] = method
            #print('CREATED '+method.Name+repr(method.Parameters))
            return method
        else:
            return self.methodsByName[name]

    def getAttach(self):
        return self.getOrCreateMethod('Attach', ['c:Creature'])

    def getDetach(self):
        return self.getOrCreateMethod('Detach', ['c:Creature'])

    def addImport(self, imp):
        if imp not in self.imports: self.imports.append(imp)

class Perk(StorageClass):
    def __init__(self):
        super().__init__()
        self.perkName = ''
        self.perkNames = set()
        self.perkDescription = ''
        self.classLimit = ''
        self.levelLimit = 0
        self.autoGained = False

    def serialize(self):
        o = super().serialize()
        o['perkName'] = self.perkName
        o['perkNames'] = self.perkNames
        o['perkDescription'] = self.perkDescription
        o['classLimit'] = self.classLimit
        o['levelLimit'] = self.levelLimit
        o['autoGained'] = self.autoGained
        return o

    def _write(self, filename):
        safename = self.GetSafeFilename()
        with codecs.open(filename, 'w', encoding='utf-8-sig') as f:
            w = IndentWriter(f, variables={
                'CLASSNAME': safename,
                'PERKID': self.ID,
                'PERKNAME': self.perkName,
                'ALLPERKNAMES': '[{}]'.format(', '.join(['"{}"'.format(x) for x in self.perkNames])),
                'PERKDESC': self.perkDescription,
                'CLASSLIMIT': self.classLimit,
                'LEVELLIMIT': self.levelLimit
            })
            w.writeline('/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**')
            w.writeline(' *')
            w.writeline(' * Updated {}'.format(date.today().strftime('%x')))
            w.writeline(' * Invoked:')
            for invFile, invLineNum, invLine, fnc in self.invokes:
                w.writeline(' * - {0}:{1} ({3}): {2}'.format(invFile, invLineNum, u"(DEFINED)" if invLine is None else invLine.replace('*/', '* /'), fnc))
            w.writeline(' * Checked:')
            for invFile, invLineNum, invLine in self.checks:
                w.writeline(' * - {0}:{1}: {2}'.format(invFile, invLineNum, u"(DEFINED)" if invLine is None else invLine.replace('*/', '* /')))
            if len(self.checks) > 0 and len(self.invokes) == 0:
                w.writeline(' * WARNING: {0} checks, but 0 invocations!  This perk may be obsolete or broken!'.format(len(self.checks)))
            w.writeline(' */')
            w.writeline()
            with w.writeline('package classes.GameData.PerkClasses {'):
                for imp in self.imports:
                    w.writeline('import {};'.format(imp))
                with w.writeline('public class {CLASSNAME} extends PerkData {'):
                    with w.writeline('public function {CLASSNAME}() {'):
                        w.writeline('setID("{PERKID}");')
                        w.writeline('perkName = "{PERKNAME}";')
                        w.writeline('setAllNames({ALLPERKNAMES});')
                        w.writeline('perkDescription = "{PERKDESC}";')
                        if self.classLimit != '':
                            w.writeline('classLimit = {CLASSLIMIT};')
                        if self.levelLimit != 0:
                            w.writeline('levelLimit = {LEVELLIMIT};')
                        if self.autoGained:
                            w.writeline('autoGained = true;')
                        w.writeline('setStorageValues({});'.format(', '.join([str(x) for x in self.values])))
                        for storageID, storageName in self.storageInfo.items():
                            w.writeline('setStorageName({}, "{}");'.format(storageID - 1, storageName))
                    w.writeline('}\n')

                    with w.writeline('override public function Attach(c:Creature):void {'):
                        w.writeline('super.Attach(c);')
                        if 'Attach' in self.methodsByName:
                            self.writeMethod(w, self.methodsByName['Attach'])
                        else:
                            w.writeline('// Add event listeners here.')
                    w.writeline('}\n')

                    with w.writeline('override public function Detach(c:Creature):void {'):
                        w.writeline('super.Detach(c);')
                        if 'Detach' in self.methodsByName:
                            self.writeMethod(w, self.methodsByName['Detach'])
                        else:
                            w.writeline('// Remove event listeners here.')
                    w.writeline('}\n')

                    with w.writeline('override public function Activate(c:Creature):void {'):
                        w.writeline('super.Activate(c);')
                        if 'Activate' in self.methodsByName:
                            self.writeMethod(w, self.methodsByName['Activate'])
                        else:
                            w.writeline('// Called when added to a creature.')
                    w.writeline('}\n')

                    with w.writeline('override public function Deactivate(c:Creature):void {'):
                        w.writeline('super.Deactivate(c);')
                        if 'Deactivate' in self.methodsByName:
                            self.writeMethod(w, self.methodsByName['Deactivate'])
                        else:
                            w.writeline('// Called when removed from a creature.')
                    w.writeline('}\n')

                    with w.writeline('override public function clone(sc:StorageClass):PerkData {'):
                        w.writeline('var pd:{CLASSNAME} = new {CLASSNAME}();')
                        w.writeline('return _clone(sc, pd);')
                    w.writeline('}\n')

                    if len(self.methods) > 0:
                        for method in self.methods:
                            if method.IsSpecial:
                                continue
                            w.writeline('// Automatically generated (definition at {}:{})'.format(method.Filename, method.LineNumber))
                            with w.writeline('private function {}({}):{} {{'.format(method.Name, ', '.join([str(x) for x in method.Parameters]),method.ReturnType)):
                                w.writeline('if(DEBUG) trace(perkName+" - {}!")'.format(method.Name))
                                lines = []
                                for i, line in enumerate(method.Lines):
                                    lines.append(method.RunReplacements(line,i))
                                writeReindentedViaIndentWriter(w, lines, offset=w.indent_level)
                            w.writeline('}\n')
                w.writeline('}')
            w.writeline('}\n')
        ensureConditionalImports(filename, KNOWN_PACKAGES, sort=True)
        unfuckFile(filename)

    def Save(self):
        safename = self.GetSafeFilename()
        fn = os.path.join(PERKBASEDIR, safename)
        if os.path.exists(fn + '.as'):
            encoding = detect_encoding(fn+'.as')
            with codecs.open(fn + '.as', 'r', encoding=encoding) as f:
                for line in f:
                    #line=line.decode('utf-8-sig').encode('utf-8')
                    if '@NO AUTOGEN@' in line and 'AUTOGENERATED' not in line:
                        log.info('Skipping %s. (@NO AUTOGEN@ is set)', fn + '.as')
                        self._write(fn + '.txt')
                        return
        with log.info('Creating %s...', fn + '.as'):
            self._write(fn + '.txt')
            # if self.classLimit=='' and os.path.exists(fn+'.as'):
            #    log.info('File already exists, skipping. (CONSIDER @NO AUTOGEN@!)')
            #    return
            shutil.move(fn + '.txt', fn + '.as')

class StatusEffect(StorageClass):
    def __init__(self):
        super().__init__()
        self.statusName = ''
        self.statusNames = set()
        self.iconName = ''
        self.tooltip = ''
        self.combatOnly = ''
        self.minutesLeft = ''
        self.iconShade = ''
        self.hidden = ''

    def serialize(self):
        o = super().serialize()
        o['statusName'] = self.statusName
        o['statusNames'] = self.statusNames
        o['iconName'] = self.iconName
        o['tooltip'] = self.tooltip
        o['combatOnly'] = self.combatOnly
        o['minutesLeft'] = self.minutesLeft
        o['iconShade'] = self.iconShade
        o['hidden'] = self.hidden
        return o

    def _write(self, filename):
        safename = self.GetSafeFilename()
        with codecs.open(filename, 'w', encoding='utf-8-sig') as f:
            w = IndentWriter(f, variables={
                'CLASSNAME': safename,
                'STATUSID': self.ID,
                'STATUSNAME': self.statusName,
                'ALLSTATUSNAMES': '[{}]'.format(', '.join(['"{}"'.format(x) for x in self.statusNames])),
                'TOOLTIP': self.tooltip,
                'ICONNAME': self.iconName,
                'ICONSHADE': self.iconShade,
                'COMBATONLY': self.combatOnly,
                'HIDDEN': self.hidden,
            })
            w.writeline('/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**')
            w.writeline(' *')
            w.writeline(' * Updated {}'.format(date.today().strftime('%x')))
            w.writeline(' * Invoked:')
            for invFile, invLineNum, invLine, fnc in self.invokes:
                w.writeline(' * - {0}:{1} ({3}): {2}'.format(invFile, invLineNum, u"(DEFINED)" if invLine is None else invLine.replace('*/', '* /'), fnc))
            w.writeline(' * Checked:')
            for invFile, invLineNum, invLine in self.checks:
                w.writeline(' * - {0}:{1}: {2}'.format(invFile, invLineNum, u"(DEFINED)" if invLine is None else invLine.replace('*/', '* /')))
            if len(self.checks) > 0 and len(self.invokes) == 0:
                w.writeline(' * WARNING: {0} checks, but 0 invocations!  This perk may be obsolete or broken!'.format(len(self.checks)))
            w.writeline(' */')
            w.writeline()
            with w.writeline('package classes.GameData.StatusEffects {'):
                for imp in self.imports:
                    w.writeline('import {};'.format(imp))
                with w.writeline('public class {CLASSNAME} extends StatusData {'):
                    with w.writeline('public function {CLASSNAME}() {'):
                        w.writeline('setID("{STATUSID}");')
                        w.writeline('statusName = "{STATUSNAME}";')
                        w.writeline('setAllNames({ALLSTATUSNAMES});')
                        w.writeline('tooltip = "{TOOLTIP}";')
                        w.writeline('iconName = "{ICONNAME}";')
                        w.writeline('iconShade = {ICONSHADE};')
                        w.writeline('hidden = {HIDDEN};')
                        w.writeline('setStorageValues({});'.format(', '.join([str(x) for x in self.values])))
                        for storageID, storageName in self.storageInfo.items():
                            w.writeline('setStorageName({}, "{}");'.format(storageID - 1, storageName))
                    w.writeline('}\n')

                    with w.writeline('override public function Attach(c:Creature):void {'):
                        w.writeline('super.Attach(c);')
                        if 'Attach' in self.methodsByName:
                            self.writeMethod(w, self.methodsByName['Attach'])
                        else:
                            w.writeline('// Add event listeners here.')
                    w.writeline('}\n')

                    with w.writeline('override public function Detach(c:Creature):void {'):
                        w.writeline('super.Detach(c);')
                        if 'Detach' in self.methodsByName:
                            self.writeMethod(w, self.methodsByName['Detach'])
                        else:
                            w.writeline('// Remove event listeners here.')
                    w.writeline('}\n')

                    with w.writeline('override public function Activate(c:Creature):void {'):
                        w.writeline('super.Activate(c);')
                        if 'Activate' in self.methodsByName:
                            self.writeMethod(w, self.methodsByName['Activate'])
                        else:
                            w.writeline('// Called when added to a creature.')
                    w.writeline('}\n')

                    with w.writeline('override public function Deactivate(c:Creature):void {'):
                        w.writeline('super.Deactivate(c);')
                        if 'Deactivate' in self.methodsByName:
                            self.writeMethod(w, self.methodsByName['Deactivate'])
                        else:
                            w.writeline('// Called when removed from a creature.')
                    w.writeline('}\n')

                    with w.writeline('override public function clone(sc:StorageClass):PerkData {'):
                        w.writeline('var pd:{CLASSNAME} = new {CLASSNAME}();')
                        w.writeline('return _clone(sc, pd);')
                    w.writeline('}\n')

                    if len(self.methods) > 0:
                        for method in self.methods:
                            if method.IsSpecial:
                                continue
                            w.writeline('// Automatically generated (definition at {}:{})'.format(method.Filename, method.LineNumber))
                            with w.writeline('private function {}({}):{} {{'.format(method.Name, ', '.join([str(x) for x in method.Parameters]),method.ReturnType)):
                                w.writeline('trace(statusName+" - {}!")'.format(method.Name))
                                lines = []
                                for i, line in enumerate(method.Lines):
                                    lines.append(method.RunReplacements(line,i))
                                writeReindentedViaIndentWriter(w, lines, offset=w.indent_level)
                            w.writeline('}\n')
                w.writeline('}')
            w.writeline('}\n')
        ensureConditionalImports(filename, KNOWN_PACKAGES, sort=True)
        unfuckFile(filename)

    def Save(self):
        safename = self.GetSafeFilename()
        fn = os.path.join(STATUSBASEDIR, safename)
        if os.path.exists(fn + '.as'):
            encoding = detect_encoding(fn+'.as')
            with codecs.open(fn + '.as', 'r', encoding=encoding) as f:
                for line in f:
                    #line=line.decode('utf-8-sig').encode('utf-8')
                    if '@NO AUTOGEN@' in line and 'AUTOGENERATED' not in line:
                        log.info('Skipping %s. (@NO AUTOGEN@ is set)', fn + '.as')
                        self._write(fn + '.txt')
                        return
        with log.info('Creating %s...', fn + '.as'):
            self._write(fn + '.txt')
            # if self.classLimit=='' and os.path.exists(fn+'.as'):
            #    log.info('File already exists, skipping. (CONSIDER @NO AUTOGEN@!)')
            #    return
            shutil.move(fn + '.txt', fn + '.as')


def stripstr(inp):
    return inp.strip('"')

ACCEPTABLE_PERK_NAME_CHARS=string.ascii_letters+string.digits+' '
def normPerkName(inp):
    o=''
    for c in inp:
        if c not in ACCEPTABLE_PERK_NAME_CHARS:
            continue
        o += c
    return o

def num(inp):
    if '.' in inp:
        return float(inp)
    else:
        return int(inp)

REG_PERK_OLDCODE_BLOCK_START = re.compile(re.escape('/* PERK REWORK - OLD CODE (checkPerk.py):'))
REG_PERK_REWORK_BLOCK_START = re.compile(re.escape('PERK REWORK - NEW CODE (checkPerk.py):*/'))
REG_PERK_REWORK_BLOCK_END = re.compile(re.escape('// END PERK REWORK (checkPerk.py)'))

REG_CREATE_PERK = re.compile(r'([a-zA-Z0-9_\.\[\]"]+)\.createPerk\(("[^"]+"), ?([^,\)]+), ?([^,\)]+), ?([^,\)]+), ?([^,\)]+), ?("[^"]*")?\);')
#createStatusEffect("Pussy Plugged",vagIndex,0,0,0,false,"Icon_Blocked","A plug is currently obstructing your pussy, the constant pressure on your tender areas constantly arousing your libido.\n\n<b>+100% Libido.</b>",false,0,0xB793C4);
#public function createStatusEffect(statusName: String, value1: Number = 0, value2: Number = 0, value3: Number = 0, value4: Number = 0, hidden: Boolean = true, iconName: String = "", tooltip: String = "", combatOnly: Boolean = false, minutesLeft: Number = 0, iconShade:uint = 0xFFFFFF): void {
REG_CREATE_STATUS_EFFECT = re.compile(r'(?P<prefix>[a-zA-Z0-9_\.\[\]"]+)\.createStatusEffect\((?P<name>"[^"]+")(, ?(?P<value1>[^,\)]+))?(, ?(?P<value2>[^,\)]+))?(, ?(?P<value3>[^,\)]+))?(, ?(?P<value4>[^,\)]+))?(, ?(?P<hidden>[^,\)]+))?(, ?(?P<iconName>"[^"]*"))?(, ?(?P<tooltip>"[^"]*"))?(, ?(?P<combatOnly>[^,\)]+))?(, ?(?P<minutesLeft>[^,\)]+))?(, ?(?P<iconShade>[^,\)]+))?\);')

# hasPerk("Amazonian Endurance")
REG_HASPERK = re.compile(r'([a-zA-Z0-9_\.\[\]"]+)\.hasPerk\(("[^"]+")\)')
# /** @BEGINPERKFUNC Amazonian Endurance:OnLine(int lineNumber,...)
REG_BEGIN_PERK_FUNC = re.compile(r'@BEGINPERKFUNC ([^:]+):([A-Za-z0-9_]+)\(([^\)]+)\)(:[a-zA-Z0-9_\.]+)?')
# * @ENDPERKFUNC
# */
REG_END_PERK_FUNC = re.compile(r'@ENDPERKFUNC')
# * @param lineNumber setheld
# * @replace /someting/something/
REG_PERK_DIRECTIVE = re.compile(r'@([a-z][a-zA-Z0-9]+) ?(.*)$')
'''
// Perk: 'Nuki Nuts
// v1: mLs of excess seed
// v2: 0 is natural (race-specific), 1 is not natural (permanent)
'''
REG_PERK_COMMENT_HEADER = re.compile(r'// Perk: ([a-zA-Z0-9\-\'" ]+)$')
REG_PERK_VDATA_INFO = re.compile(r'// v([1-4]): (.*)$')

def assert_equal(_input, _expected, _actual):
    if _expected == _actual:
        return
    with log.info('Input:'):
        log.info(_input)
    with log.info('Expected Output:'):
        log.info(_expected)
    with log.info('Actual Output:'):
        log.info(_actual)
    raise ValueError()

def test_001_arrays():
    pf = PerkFinder()
    pf.ResetStateMachines()
    line = 'var arr:Array = new Array("one", "two", "three", "four", "five", "six");'
    expected = 'var arr:Array = new Array("one", "two", "three", "four", "five", "six");'
    actual = pf.ParseLine(line).strip()
    #assert actual == expected, actual
    assert_equal(line,expected,actual)

def test_002_complex_variables():
    pf = PerkFinder()
    pf.NO_BACKUP = True
    pf.ResetStateMachines()
    line = 'if(!chars["ANNO"].hasPerk("Regal Mane")) annoToHuskarTF();'
    expected = 'if(!PerkRegistry.REGAL_MANE.hasPerk(chars["ANNO"])) annoToHuskarTF();'
    '''
     /* PERK REWORK - OLD CODE (checkPerk.py):
if(!chars["ANNO"].hasPerk("Regal Mane")) annoToHuskarTF();
   PERK REWORK - NEW CODE (checkPerk.py):*/
if(!chars["ANNO"]PerkRegistry.REGAL_MANE.hasPerk(this)) annoToHuskarTF();
// END PERK REWORK (checkPerk.py)
    '''
    actual = pf.ParseLine(line).strip()
    #assert actual == expected, actual
    assert_equal(line,expected,actual)

def test_003_anonymous_functions():
    pf = PerkFinder()
    pf.NO_BACKUP = True
    pf.ResetStateMachines()
    line = 'if (!force) addButton(13, "Override", function():* { clearOutput(); routeMenu(target, true); }, null, "Override", "Enable override mode. The system would attempt to execute route without checking its availability.\nWarning: this menu is for authorized personnel only.\nWarning: bypassing of safety protocols is not advised, those functions are called unsafe for a reason.");'
    expected = line
    actual = pf.ParseLine(line).strip()
    #assert actual == expected, actual
    assert_equal(line,expected,actual)

def asNumOr0(inp):
    try:
        return num(inp)
    except ValueError:
        return 0

class PerkFinder:

    def __init__(self):
        self.perks = {}
        self.statusEffects = {}
        self.events = {}

        self.mappings = {
            'classLimit': str,
            'levelLimit': int,
            'autoGained': bool,
            'perkName': stripstr,
            'perkDescription': stripstr,
        }

        self.currentMethod = None
        self.currentPerk = None
        self.currentLine = ''
        self.currentPerkData = None
        self.lastLine = ''
        self.changes = 0

        self.currentLineNumber=0
        self.currentFileName=''
        self.numFiles=0
        self.currentFileNum=0

        self.files=[]
        self.excludedPathElements=[]

        self.DEBUG_WRITE = False
        self.NO_BACKUP = False

    def matches(self, line, oline, regex, action=None):
        m = regex.search(line)
        if m is not None:
            if action != None:
                action(self.currentFileName, self.currentLineNumber, line, oline, m)
            return True
        return False

    def all_matches(self, line, oline, regex, action):
        for m in regex.finditer(line):
            if action != None:
                action(self.currentFileName, self.currentLineNumber, line, oline, m)

    def PerksParse(self, filename):
        '''
                var criticalBlows:PerkData = new PerkData();
                criticalBlows.classLimit = GLOBAL.CLASS_MERCENARY;
                criticalBlows.levelLimit = 2;
                criticalBlows.autoGained = true;
                criticalBlows.perkName = "Critical Blows";
                criticalBlows.perkDescription = "Your strikes and shots gain a 10% chance of inflicting double damage on normal melee and ranged attacks.";
                insertPerkData(criticalBlows);
        '''
        perk = None
        REG_PERKLINE = re.compile(r'([^\.]+)\.([a-zA-Z]+) = ([^;]+);')
        encoding = detect_encoding(filename)
        with codecs.open(filename, 'r', encoding=encoding) as f:
            self.currentLineNumber = 0
            for line in f:
                self.currentLineNumber += 1
                line = line.strip()
                if perk is None:
                    if line.endswith(':PerkData = new PerkData();'):
                        perk = Perk()
                else:
                    if line.startswith('insertPerkData('):
                        perk.invokes = [(filename, self.currentLineNumber, None, 'PerksParse')]
                        self.perks[perk.ID] = perk
                        log.info('%s%s:%d: Found %s!', self._logPrefix(), filename, self.currentLineNumber, perk.ID)
                        perk = None
                        continue
                    m = REG_PERKLINE.match(line)
                    if m is not None:
                        varName, value = m.group(2, 3)
                        if varName == 'perkName':
                            vstr = self.mappings[varName](value)
                            perk.ID=normPerkName(vstr)
                            perk.perkName=vstr
                            perk.perkNames.add(vstr)
                        setattr(perk, varName, self.mappings[varName](value))

    def _logPrefix(self):
        return '[{}/{}] '.format(self.currentFileNum,self.numFiles) if self.numFiles > 0 else ''

    def _parsedPerkCreationFindOnly(self, filename, ln, line, oline, m):
        p = Perk()
        p.perkName = stripstr(m.group(2))
        p.perkNames.add(p.perkName)
        p.ID = normPerkName(p.perkName)
        p.values[0] = asNumOr0(m.group(3))
        p.values[1] = asNumOr0(m.group(4))
        p.values[2] = asNumOr0(m.group(5))
        p.values[3] = asNumOr0(m.group(6))
        if m.lastindex == 7:
            p.perkDescription = stripstr(m.group(7))
        if p.ID not in self.perks:
            self.perks[p.ID] = p
            log.info('%s%s:%d: Found %s!', self._logPrefix(), filename, ln, p.ID)
        else:
            kp = self.perks[p.ID]
            if kp.placeholder:
                kp.perkDescription = p.perkDescription
                p.values[0] = asNumOr0(m.group(3))
                p.values[1] = asNumOr0(m.group(4))
                p.values[2] = asNumOr0(m.group(5))
                p.values[3] = asNumOr0(m.group(6))
        self.perks[p.ID].invokes += [(filename, ln, line, '_parsedPerkCreationFindOnly')]

    def _parsedStatusEffectCreationFindOnly(self, filename, ln, line, oline, m):
        #log.info('STATUS EFFECT: %r', m.groupdict())
        '''
        {
            'prefix': 'victim',
            'name': '"Undetected Furpies"',
            'value1': '0',
            'value2': '0',
            'value3': '0',
            'value4': '0',
            'hidden': 'true',
            'iconName': '""',
            'tooltip':
            '"Hidden furpies infection! OH NOEZ"',
            'combatOnly': 'false',
            'minutesLeft': '17280',
            'iconShade': '0xFF69B4'
        }
        '''
        p = StatusEffect()
        p.statusName = stripstr(m.group('name'))
        p.statusNames.add(p.statusName)
        p.ID = normPerkName(p.statusName)
        p.values[0] = asNumOr0(m.group('value1') or '0')
        p.values[1] = asNumOr0(m.group('value2') or '0')
        p.values[2] = asNumOr0(m.group('value3') or '0')
        p.values[3] = asNumOr0(m.group('value4') or '0')
        p.hidden = m.group('hidden') or 'true'
        p.iconName = m.group('iconName') or ''
        p.tooltip = m.group('tooltip') or ''
        p.combatOnly = m.group('combatOnly') or 'false'
        p.minutesLeft = m.group('minutesLeft') or '0'
        p.iconShade = m.group('iconShade') or '0xFFFFFF'

        if p.ID not in self.statusEffects:
            self.statusEffects[p.ID] = p
            log.info('%s%s:%d: Found %s!', self._logPrefix(), filename, ln, p.ID)
        else:
            kp = self.statusEffects[p.ID]
            if kp.placeholder:
                kp.perkDescription = p.perkDescription
                p.values[0] = asNumOr0(m.group(3))
                p.values[1] = asNumOr0(m.group(4))
                p.values[2] = asNumOr0(m.group(5))
                p.values[3] = asNumOr0(m.group(6))
        self.statusEffects[p.ID].invokes += [(filename, ln, line, '_parsedStatusEffectCreationFindOnly')]

    def _subCreatePerk(self, m):
        #print(repr(m.groups()))
        varName = m.group(1).rstrip('.')
        if varName == '':
            varName = 'this'
        perkName = stripstr(m.group(2))
        self.changes += 1
        #self._parsedPerkCreationFindOnly(self.currentFileName,self.currentLineNumber,self.currentLine,m)
        return 'PerkRegistry.{}.applyTo({});'.format(makeConstName(perkName), varName)

    def _parsedHasPerkFindOnly(self, filename, ln, line, oline, m):
        p = Perk()
        p.perkName = stripstr(m.group(2))
        p.perkNames.add(p.perkName)
        p.ID = normPerkName(p.perkName)
        p.placeholder = True
        if p.ID not in self.perks:
            self.perks[p.ID] = p
            log.info('%s%s:%d: Found %s!', self._logPrefix(), filename, ln, p.ID)
        self.perks[p.ID].checks += [(filename, ln, line)]
        return self.perks[p.ID]

    def _subHasPerk(self, m):
        varName = m.group(1).rstrip('.')
        if varName == '':
            varName = 'this'
        self.changes += 1
        #self._parsedHasPerkFindOnly(self.currentFileName,self.currentLineNumber,self.currentLine,m)
        return 'PerkRegistry.{}.hasPerk({})'.format(makeConstName(m.group(2)), varName)

    # def _parsedHasPerkReplace(self,filename,ln,line,m):
    #    self._parsedHasPerkFindOnly(filename,ln,line,m)
    #    self.changes += 1

    def _parsedPerkBeginWRONG(self, filename, ln, line, oline, m):
        log.critical('%s%s:%d: Found %s, already in %s (starts at line %d)!', self._logPrefix(), filename, ln, m.group(1), self.currentMethod.Name, self.currentMethod.LineNumber)
        sys.exit(1)

    def _parsedPerkBegin(self, filename, ln, line, oline, m):
        # print('BEGINPERKFUNC')
        p = Perk()
        p.perkName = stripstr(m.group(1))
        p.perkNames.add(p.perkName)
        p.ID = normPerkName(p.perkName)
        p.placeholder = True
        method = PerkMethod()
        method.Name = m.group(2)
        method.Filename = filename
        method.LineNumber = ln
        for x in m.group(3).split(','):
            xp = PerkMethodParameter(x.strip())
            method.Parameters.append(xp)
            method.ParametersByName[xp.Name] = xp
        method.ReturnType=(m.group(4) or 'void').strip(':')
        if p.ID not in self.perks:
            self.perks[p.ID] = p
            log.info('%s%s:%d: Found %s!', self._logPrefix(), filename, ln, p.ID)
        if method.Name in self.perks[p.ID].methodsByName:
            method=self.perks[p.ID].methodsByName[method.Name]
        else:
            self.perks[p.ID].methods.append(method)
            self.perks[p.ID].methodsByName[method.Name] = method
        self.currentMethod = method
        self.currentPerk = self.perks[p.ID]

    def _parsedPerkEnd(self, filename, ln, line, oline, m):
        # print('ENDPERKFUNC')
        self.currentMethod = None

    def _parsedPerkDataHeader(self, filename, ln, line, oline, m):
        p = Perk()
        p.perkName = stripstr(m.group(1).strip('"').strip())
        p.perkNames.add(p.perkName)
        p.ID = normPerkName(p.perkName)
        p.placeholder = True
        if p.ID not in self.perks:
            self.perks[p.ID] = p
            log.info('%s%s:%d: Found %s!', self._logPrefix(), filename, ln, p.ID)
        self.currentPerkData = self.perks[p.ID]
        self.currentPerk = None

    def _parsedPerkStorageName(self, filename, ln, line, oline, m):
        # print(repr(m.groups()))
        vID = int(m.group(1))
        vInfo = m.group(2).strip('"').strip()
        self.currentPerkData.storageInfo[vID] = vInfo

    def _parsedPerkDirective(self, filename, ln, line, oline, m):
        if self.currentMethod is None:
            return
        directive = m.group(1).lower()
        #print('PERKFUNCDIRECTIVE '+directive+" "+m.group(2))
        if directive == 'rename':
            params = m.group(2).split(' ')
            if params[0] == 'varprefix':
                self.currentMethod.CodeReplacements += [(r'\b'+params[1]+r'.\b', params[2]+'.','r')]
                return
            if params[0] == 'variable':
                self.currentMethod.CodeReplacements += [(r'\b'+params[1]+r'\b', params[2],'r')]
                return
        if directive == 'param':
            params = m.group(2).split(' ')
            target = self.currentMethod.ParametersByName[params[0]]
            if params[1] == 'setheld':
                self.currentMethod.CodeReplacements += [(r'\b'+target.Name+r'\b', target.Name + '.value','r')]
                return
            if params[1] == 'rename':
                self.currentMethod.CodeReplacements += [(r'\b'+target.Name+r'\b', params[2],'r')]
                return
        elif directive == 'import':
            # @import classes.Engine.Interfaces.AddLogEvent
            self.currentPerk.addImport(m.group(2).strip())
        elif directive == 'ignoreline':
            return # :^)
        elif directive == 'appendcode':
            lineIndent = getIndentChars(oline)
            self.currentMethod.Lines += [lineIndent+m.group(2)]
            self.currentMethod.AppendedLines += [len(self.currentMethod.Lines)-1]
            return
        elif directive == 'replace':
            delim = m.group(2)[0]
            # replace /original/replacement/opts
            params = m.group(2).replace('\\b',r'\b').split(delim)
            #print('{!r} -> {!r} = {!r}'.format(delim,m.group(2),[params[1],params[2]]))
            # sys.exit(1)
            self.currentMethod.Replacements += [(params[1], params[2], params[3])]
        elif directive == 'remove':
            remove = m.group(2)
            self.currentMethod.Replacements += [(remove, '','')]
        elif directive == 'eventhook':
            # print(repr(m.group(2)))
            # @eventhook c.SomethingOrOther
            # c.SomethingOrOther.Attach(this, ...)
            self.currentPerk.getAttach().Lines.append('{}.Attach(this, {});'.format(m.group(2), self.currentMethod.Name))
            self.currentPerk.getDetach().Lines.append('{}.Detach(this, {});'.format(m.group(2), self.currentMethod.Name))
        else:
            log.error('UNKNOWN DIRECTIVE AT %s:%d: %s', filename, ln, directive)

    def ResetStateMachines(self):
        self.currentLineNumber = 0
        self.inReworkBlock = False
        self.inComment = False
        self.inOldCodeBlock = False
        self.inPerkDataHeader = False
        self.DEBUG_WRITE = False

    def Parse(self, filename):

        self.changes = 0
        self.currentFileName = filename
        pathchunks = os.path.normpath(filename).split(os.sep)
        for i in range(len(pathchunks)):
            if pathchunks[i].lower() in ('includes-fixed', 'classes-fixed', 'avmplus'):
                return
            if pathchunks[i].lower() in ('includes', 'classes'):
                pathchunks[i] = pathchunks[i] + '-fixed'
        fixedfile = os.sep.join(pathchunks)
        fixeddir = os.path.dirname(fixedfile)
        if not os.path.isdir(fixeddir):
            os.makedirs(fixeddir)

        encoding = detect_encoding(filename)
        with codecs.open(filename, 'r', encoding=encoding) as f:
            with codecs.open(fixedfile, 'w', encoding='utf-8-sig') as w:
                #ln = 0
                #inReworkBlock = False
                #inComment = False
                #inOldCodeBlock = False
                #inPerkDataHeader = False
                #DEBUG_WRITE = False
                self.ResetStateMachines()
                for line in f:
                    self.currentLineNumber += 1
                    w.write(self.ParseLine(line))
        ensureImportsExist(fixedfile, ['classes.GameData.PerkRegistry'])
        if 'devTools' in fixedfile:
            print(fixedfile)
            sys.exit(1)
        if self.changes == 0 and not self.DEBUG_WRITE:
            os.remove(fixedfile)
        else:
            log.info('%s%d changes written to %s', self._logPrefix(), self.changes, fixedfile)
            unfuckFile(fixedfile)

    def DumpPerksTo(self, filename):
        allperks={}
        for perkID, perk in self.perks.items():
            allperks[perkID]=perk.serialize()
        with open(filename, 'w') as f:
            yaml.dump(allperks, f, default_flow_style=False)

    def DumpStatusEffectsTo(self, filename):
        allperks={}
        for seID, statusEffect in self.statusEffects.items():
            allperks[seID]=statusEffect.serialize()
        with open(filename, 'w') as f:
            yaml.dump(allperks, f, default_flow_style=False)

    def ParseLine(self, line):
        oline = line
        self.lastLine = self.currentLine
        self.currentLine = line.lstrip().strip('\r\n')
        indent = getIndentChars(oline)
        line = line.strip()

        # // Perk: hurrr
        if self.matches(line, oline, REG_PERK_COMMENT_HEADER, self._parsedPerkDataHeader):
            if self.inPerkDataHeader:
                log.critical('%s:%d: Hit perk info header when already in perk info block!', self.currentFileName, self.currentLineNumber)
                log.critical('%s:%d: Oldcode block started here.', self.perkinfoStart[0], self.perkinfoStart[1])
                sys.exit(1)
            self.perkinfoStart=(self.currentFileName, self.currentLineNumber)
            self.inPerkDataHeader = True
            return '{}{}\n'.format(indent, self.currentLine)
        if self.inPerkDataHeader and self.matches(line, oline, REG_PERK_VDATA_INFO, self._parsedPerkStorageName):
            return '{}{}\n'.format(indent, self.currentLine)
        self.currentPerkData = None
        self.inPerkDataHeader = False

        if self.matches(line, oline, REG_PERK_REWORK_BLOCK_END):
            self.inReworkBlock = False
            self.inOldCodeBlock = False
            # DEBUG_WRITE=True
            #print('REG_PERK_REWORK_BLOCK_END: '+line.strip())
            return ''
        if self.matches(line, oline, REG_PERK_OLDCODE_BLOCK_START):
            if self.inOldCodeBlock:
                log.critical('%s:%d: Hit oldcode start block when already in oldcode block!', self.currentFileName, self.currentLineNumber)
                log.critical('%s:%d: Oldcode block started here.', self.oldcodeStart[0], self.oldcodeStart[1])
                sys.exit(1)
            self.oldcodeStart=(self.currentFileName, self.currentLineNumber)
            self.inOldCodeBlock = True
            self.inReworkBlock = False
            # print('REG_PERK_OLDCODE_BLOCK_START')
            return ''
        if self.matches(line, oline, REG_PERK_REWORK_BLOCK_START):
            if not self.inOldCodeBlock:
                log.warn('REG_PERK_OLDCODE_BLOCK_START not present, continuing.')
            self.inReworkBlock = True
            self.inOldCodeBlock = False
            # print('REG_PERK_REWORK_BLOCK_START')
            return ''
        if self.inReworkBlock:
            return ''

        lastChanges = self.changes
        if line.startswith('/*'):
            self.inComment = True
        if '*/' in line:
            self.inComment = False
        if self.currentMethod is None:
            # @BEGINPERKFUNC
            if self.matches(line, oline, REG_BEGIN_PERK_FUNC, self._parsedPerkBegin):
                self.perkmethodStart=(self.currentFileName, self.currentLineNumber)
                return '{}{}\n'.format(indent, self.currentLine)
        else:
            if self.matches(line, oline, REG_END_PERK_FUNC,  self._parsedPerkEnd) \
            or self.matches(line, oline, REG_PERK_DIRECTIVE, self._parsedPerkDirective) \
            or self.matches(line, oline, REG_BEGIN_PERK_FUNC, self._parsedPerkBeginWRONG):
                return '{}{}\n'.format(indent, self.currentLine)
            self.currentMethod.Lines += [oline]
        if self.currentMethod is None and not self.inComment and not line.startswith('//'):
            origLine = self.currentLine
            self.currentLine = REG_HASPERK.sub(self._subHasPerk, self.currentLine)
            self.currentLine = REG_CREATE_PERK.sub(self._subCreatePerk, self.currentLine)
            if origLine != self.currentLine:
                self.changes += 1
        self.all_matches(line, oline, REG_CREATE_PERK, self._parsedPerkCreationFindOnly)
        self.all_matches(line, oline, REG_CREATE_STATUS_EFFECT, self._parsedStatusEffectCreationFindOnly)
        self.all_matches(line, oline, REG_HASPERK, self._parsedHasPerkFindOnly)
        out = ''
        if not self.NO_BACKUP and lastChanges < self.changes:
            out += indent + '/* PERK REWORK - OLD CODE (checkPerk.py):\n'
            out += '{}{}\n'.format(indent,line.replace('*/', '* /'))
            out += indent + '   PERK REWORK - NEW CODE (checkPerk.py):*/\n'
        out += '{}{}\n'.format(indent,self.currentLine)
        if not self.NO_BACKUP and lastChanges < self.changes:
            out += indent + '// END PERK REWORK (checkPerk.py)\n'
        return out

    def ExcludePathElement(self, pathel):
        self.excludedPathElements += [pathel]

    def FindFilesIn(self, path):
        log.info('Finding files in %s...', path)
        #self.files = []
        for root, _, files in os.walk(path):
            for filename in files:
                filepath = os.path.join(root, filename)
                if filepath.endswith('.bak') or filepath.endswith('.fixed'):
                    if os.path.isfile(filepath):
                        os.remove(filepath)
                        print('RM '+filepath)
                    continue
                if not filename.endswith('.as'):
                    continue
                bad = False
                for pathel in filepath.split(os.sep):
                    if pathel in self.excludedPathElements:
                        bad=True
                        break
                if bad:
                    continue
                self.files += [filepath]
        print(len(self.files))

    def Process(self):
        self.numFiles=len(self.files)
        self.currentFileNum=0
        for filepath in self.files:
            self.currentFileNum+=1
            #log.info('Reading %s...',filepath)
            if filepath == 'Perks.as':
                self.PerksParse(filepath)
            else:
                self.Parse(filepath)

if __name__ == '__main__':
    test_001_arrays()
    test_002_complex_variables()
    test_003_anonymous_functions()
    main()
