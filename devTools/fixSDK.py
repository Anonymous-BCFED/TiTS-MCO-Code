import os
import sys
import shutil
import re

from semantic_version import Version

from typing import Optional
from lxml import etree
from buildtools.os_utils import ENV
from buildtools import os_utils
from buildtools import log
from buildtools.config import YAMLConfig
from buildtools.indentation import IndentWriter

IMAJOR = 0
IMINOR = 1
IPATCH = 2
IRELEASE = 3

JAVA_MAX_HEAP='2g'

def list_cmp(a,b):
    for i in min(len(a),len(b)):
        cr = cmp(a,b)
        if cr != 0:
            return cr
    return 0

# Max version of Adobe AIR.
# 25.0.4 would be [25,0,4].
MAX_AIR=[999,0,0]


IMAJOR = 0
IMINOR = 1
IPATCH = 2
IRELEASE = 3

# jdk-11.0.1
# jre1.8.0_191
REG_3PARTS=re.compile(r'(?P<type>jdk|jre)\-?(?P<major>\d+)\.(?P<minor>\d+)\.(?P<patch>\d+)$')
REG_4PARTS=re.compile(r'(?P<type>jdk|jre)\-?(?P<major>\d+)\.(?P<minor>\d+)\.(?P<patch>\d+)(?:[\._])(?P<build>\d+)$')

class JEnvVersion(object):

    def __init__(self, versionName, fullpath):
        self.is_jdk = False
        self.major=0
        self.minor=0
        self.patch=0
        self.build=-1
        self.path=''
        self.fullpath = fullpath
        self.x64='Program Files (x86)' not in fullpath
        self.path = versionName

        for regex in [REG_3PARTS, REG_4PARTS]:
            m = regex.match(versionName)
            if m is not None:
                g = m.groupdict()
                self.major = int(g['major'])
                self.minor = int(g['minor'])
                self.patch = int(g['patch'])
                self.build = int(g.get('build','-1'))
                self.is_jdk = g.get('type')=='jdk'
                break

    def __str__(self) -> str:
        # JDK 1.2.3.4
        versionparts = [self.major, self.minor, self.patch]
        if self.build > -1:
            versionparts += [self.build]
        return '{} {} ({})'.format('JDK' if self.is_jdk else 'JRE', '.'.join([str(x) for x in versionparts]), self.fullpath)

    def __eq__(self, other):
        return self.is_jdk == other.is_jdk \
            and self.major == other.major \
            and self.minor == other.minor \
            and self.patch == other.patch \
            and self.build == other.build

    def __gt__(self, other):
        if self.is_jdk and not other.is_jdk:
            return True
        return self.major > other.major \
            or self.minor > other.minor \
            or self.patch > other.patch \
            or self.build > other.build

    def __lt__(self, other):
        if not self.is_jdk and other.is_jdk:
            return True
        return self.major < other.major \
            or self.minor < other.minor \
            or self.patch < other.patch \
            or self.build < other.build

    def __ge__(self, other):
        if self.is_jdk and not other.is_jdk:
            return True
        return self.major >= other.major \
            or self.minor >= other.minor \
            or self.patch >= other.patch \
            or self.build >= other.build

    def __le__(self, other):
        if not self.is_jdk and other.is_jdk:
            return True
        return self.major <= other.major \
            or self.minor <= other.minor \
            or self.patch <= other.patch \
            or self.build <= other.build

    def __ne__(self, other):
        return self.is_jdk != other.is_jdk \
            or self.major != other.major \
            or self.minor != other.minor \
            or self.patch != other.patch \
            or self.build != other.build

class FlexAirSDKVersion(object):
    def __init__(self, version, fullpath):
        self.path = fullpath
        self.air_version = None
        self.flex_version = None
        if version is not None:
            flex_version_str, air_sdk_str = version.split('+')
            self.flex_version = self.versionFromString(flex_version_str)
            self.air_version = self.versionFromString(air_sdk_str)
        else:
            self.flex_version = self.readFlexVersionFrom(fullpath)
            self.air_version = self.readAirVersionFrom(fullpath)
        self._cmp_flex_version = self.genCmpVersionFrom(self.flex_version)
        self._cmp_air_version = self.genCmpVersionFrom(self.air_version)
        log.info('Found flex=%r, air=%r', self.flex_version, self.air_version)

    def genCmpVersionFrom(self, ver):
        if ver is not None:
            return Version(self._tuple2str(ver))
        else:
            return None

    def readAirVersionFrom(self, dirname):
        filename = os.path.join(dirname, 'air-sdk-description.xml')
        if os.path.isfile(filename):
            return self.getVersionFromDescription(filename)
        else:
            return None

    def readFlexVersionFrom(self, dirname):
        filename = os.path.join(dirname, 'flex-sdk-description.xml')
        if os.path.isfile(filename):
            return self.getVersionFromDescription(filename)
        else:
            return None

    def versionFromString(self, instr: str) -> list:
        return [int(x) for x in instr.split('.')]

    def getVersionFromDescription(self, filename):
        from lxml import etree
        root = etree.parse(filename)
        #name = root.find('name').text.strip()
        version = self.versionFromString(root.find('version').text.strip())
        #build = int(root.find('build').text.strip())
        return version

    def _tuple2str(self, t: tuple) -> str:
        return '.'.join([str(x) for x in t])

    def __str__(self):
        return self.toStr()

    def toStr(self, prefix: bool = None, sep: str = '+'):
        # false,'+': 1.2.3+4.5.6
        # true,',': Flex 1.2.3, Air 4.5.6
        o=[]
        if prefix is None:
            prefix = self.air_version is None or self.flex_version is None
        if self.flex_version:
            o += [('Flex ' if prefix else '')+self._tuple2str(self.flex_version)]
        if self.air_version:
            o += [('Air ' if prefix else '')+self._tuple2str(self.air_version)]
        return sep.join(o)

    def __eq__(self, other):
        return self.path == other.path \
            and self._cmp_flex_version == other._cmp_flex_version \
            and self._cmp_air_version == other._cmp_air_version

    def __ne__(self, other):
        return self.path != other.path \
            or self._cmp_flex_version != other._cmp_flex_version \
            or self._cmp_air_version != other._cmp_air_version

    def __gt__(self, other):
        return self._cmp_flex_version > other._cmp_flex_version \
            or self._cmp_air_version > other._cmp_air_version

    def __lt__(self, other):
        return self._cmp_flex_version < other._cmp_flex_version \
            or self._cmp_air_version < other._cmp_air_version

    def __ge__(self, other):
        return self._cmp_flex_version >= other._cmp_flex_version \
            or self._cmp_air_version >= other._cmp_air_version

    def __le__(self, other):
        return self._cmp_flex_version <= other._cmp_flex_version \
            or self._cmp_air_version <= other._cmp_air_version

def patch_as3proj(filename, sdkID, outputfile=None):
    if outputfile is None:
        outputfile = filename
    with log.info('Patching %s...', filename):
        root = etree.parse(filename)
        #print(etree.tostring(root))
        #if root.attrib['version'] == '2':
        found=False
        for match in root.xpath('//movie[@preferredSDK]'):
            log.info('preferredSDK: %s -> %s',match.attrib['preferredSDK'],sdkID)
            match.attrib['preferredSDK']=sdkID
            found=True
        if not found:
            for match in root.xpath('//output'):
                log.info('preferredSDK: Added as %s',sdkID)
                etree.SubElement(match, 'movie', {'preferredSDK': sdkID})
                break
        with open(outputfile+'.tmp', 'wb') as f:
            f.write(etree.tostring(root, method='xml', xml_declaration=True, pretty_print=True, encoding='utf-8'))
        log.info('Writing to %s...', outputfile)
        if os.path.isfile(outputfile):
            os.remove(outputfile)
        shutil.move(outputfile+'.tmp', outputfile)
        if os.path.isfile(outputfile+'.tmp'):
            os.remove(outputfile+'.tmp')

config = YAMLConfig('buildconfig.yml')
java_home = ENV.get('JAVA_HOME', '')
log.info('JAVA_HOME = %s', java_home)
appdata = ENV.get('LOCALAPPDATA', '')
log.info('LOCALAPPDATA = %s', java_home)

# if os.path.isfile(os.path.join(java_home,'bin','java.exe')):
#    log.info('JAVA_HOME correctly set.')
#    sys.exit(0)

log.info('Determining proper JAVA_HOME...')
java_root = 'C:\\Program Files\\Java'
versions = []
for dirname in os.listdir(java_root):
    fullpath = os.path.join(java_root, dirname)
    with log.info('Found %s', fullpath):
        if os.path.isfile(os.path.join(fullpath, 'bin', 'java.exe')):
            versions.append(JEnvVersion(dirname, fullpath))
        else:
            log.warn('%s is missing bin/java.exe. Skipped.', fullpath)
java_root = 'C:\\Program Files (x86)\\Java'
for dirname in os.listdir(java_root):
    fullpath = os.path.join(java_root, dirname)
    with log.info('Found %s', fullpath):
        if os.path.isfile(os.path.join(fullpath, 'bin', 'java.exe')):
            versions.append(JEnvVersion(dirname, fullpath))
        else:
            log.warn('%s is missing bin/java.exe. Skipped.', fullpath)
with log.info('Sorting versions...'):
    versions.sort()
jdk_versions=[]
jre_versions=[]
with log.info('Available versions:'):
    newversions = []
    for version in reversed(versions):
        if not version.x64:
            continue
        with log.info('* %s', str(version)):
            if version.is_jdk:
                jdk_versions.append(version)
            else:
                jre_versions.append(version)
    versions = newversions
new_javahome = jre_versions[0].fullpath
jdk_home = jdk_versions[0].fullpath

flexairsdk = ''
with log.info('Identifying available Flex+Air SDKs...'):
    allflexairsdks_dirs = [
        os.path.join(appdata, 'FlashDevelop', 'Apps', 'flexairsdk')
    ]
    allflexairsdks_dirs += config.get('paths.flex-sdk-search-paths', [])
    allflexairsdks = []
    for sdkpath in config.get('paths.flex-sdks', []):
        if os.path.isfile(sdkpath):
            log.warning('Skipping %s: Is a file.', sdkpath)
            continue
        sdk = FlexAirSDKVersion(None, sdkpath)
        if not sdk.air_version:
            log.warning('Skipping %s: AIR SDK not included.', sdkpath)
            continue
        if MAX_AIR < sdk.air_version:
            log.warning('Skipping %s: too new!', sdkpath)
            continue
        allflexairsdks += [sdk]

    for sdks_dir in allflexairsdks_dirs:
        for subdir in os.listdir(sdks_dir):
            sdkpath = os.path.join(sdks_dir, subdir)
            if os.path.isfile(sdkpath):
                log.warning('Skipping %s: Is a file.', sdkpath)
                continue
            sdk = FlexAirSDKVersion(subdir, sdkpath)
            if not sdk.air_version:
                log.warning('Skipping %s: AIR SDK not included.', sdkpath)
                continue
            if sdk.air_version and MAX_AIR < sdk.air_version:
                log.warning('Skipping %s: too new!', sdkpath)
                continue
            #log.info(subdir)
            allflexairsdks += [sdk]
    with log.info('Sorting versions...'):
        allflexairsdks.sort(reverse=True)
    with log.info('Available versions:'):
        for version in reversed(allflexairsdks):
            log.info('* %s', str(version))
    flexairsdk = allflexairsdks[0].path
    log.info('flexairsdk = %s', flexairsdk)

with log.info('Fixing jvm.config...'):
    jvm_config_path = os.path.join(flexairsdk, 'bin', 'jvm.config')
    if not os.path.isfile(jvm_config_path+'.bak') and os.path.isfile(jvm_config_path):
        log.info('Backed up %s', jvm_config_path)
        shutil.copy(jvm_config_path, jvm_config_path+'.bak')

    with open(jvm_config_path, 'w') as f:
        w = IndentWriter(f)
        w.writeline('# Generated by fixSDK.py')
        w.writeline('# DO NOT MODIFY UNLESS YOU KNOW WHAT YOU\'RE DOING')
        w.writeline('java.home={}'.format(new_javahome))
        w.writeline('java.args=-Xmx{} -Dsun.io.useCanonCaches=false'.format(JAVA_MAX_HEAP))
        w.writeline('env=')
        w.writeline('java.class.path=')
        w.writeline('java.library.path=')
    log.info('Generated %s', jvm_config_path)

a=FlexAirSDKVersion('4.6.0+23.0.0','.')
b=FlexAirSDKVersion('4.6.0+25.0.0','.')
c=(a>b)-(a<b)
log.info(repr(c))
assert a < b

patch_as3proj('TiTSFD.as3proj', flexairsdk, outputfile='TiTSFD-Modded.as3proj')
patch_as3proj('TiTS-SE.as3proj', flexairsdk, outputfile='TiTS-SE-Modded.as3proj')
