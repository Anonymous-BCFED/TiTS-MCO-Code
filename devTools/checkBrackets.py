import sys

class BracketFormatter:
    def __init__(self,filename):
        self.filename=filename
        self.lastLine = ''
        self.lastSLine = ''
        self.lastLineTabs = 0
        self.ln = 0

        self.line = ''
        self.s_line = ''
        self.ntabs = 0

        self.InCase=False

        self.Process()

    def updateNumTabs(self):
        deficit=0
        if self.getLineDeficit(self.s_line) < 0:
            deficit += -1
        if self.getLineDeficit(self.lastSLine) > 0:
            deficit += 1

        if self.s_line.startswith('break;') and self.InCase:
            self.InCase=False
            deficit += -1
        if self.lastSLine.startswith('case ') and not self.InCase:
            self.InCase=True
            deficit += 1
        if self.lastSLine.startswith('default:') and not self.InCase:
            self.InCase=True
            deficit += 1
        return deficit

    def getLineDeficit(self,line):
        bracketlevel=0
        for c in line:
            if c == '{':
                bracketlevel+=1
            if c == '}':
                bracketlevel-=1
        return bracketlevel

    def Process(self):
        with open(self.filename, 'r') as inf:
            with open(self.filename+'.new','w') as outf:
                for l in inf:
                    self.ln += 1
                    self.line = l.rstrip()
                    self.s_line = self.line.strip()
                    self.ntabs += self.updateNumTabs()
                    self.ntabs = max(self.ntabs,0)

                    outf.write('{tabs}{line}\n'.format(tabs='\t'*self.ntabs,line=self.s_line))
                    self.lastLine = self.line
                    self.lastSLine = self.s_line
                    self.lastLineTabs = self.ntabs

class BracketTracker:

    def __init__(self, filename):
        self.filename = filename
        self.lastLine = ''
        self.lastSLine = ''
        self.lastLineTabs = 0
        self.ln = 0

        self.line = ''
        self.s_line = ''
        self.ntabs = ''

        self.Process()

    def getNumTabs(self):
        t = 0
        for c in self.line:
            if c == '\t':
                t += 1
            elif c == '\t':
                t += 0.25
            else:
                return t

    def CheckDedent(self):
        if self.lastSLine=='' or self.s_line=='':
            return
        if self.line.endswith('}'):
            return
        if self.lastSLine.startswith('break;'):
            return
        self.throwError('DEDENT, Needs }')

    def CheckIndent(self):
        if self.lastSLine=='' or self.s_line=='':
            return
        if self.lastLine.endswith('{'):
            return
        if self.lastSLine.startswith('case '):
            return
        if self.lastSLine.startswith('default:'):
            return
        if self.lastSLine.startswith('if '):
            return
        self.throwError('INDENT next line, Needs {')

    def throwError(self, message):
        print('\t'.join([self.filename, str(self.ln), message]))

    def Process(self):
        with open(self.filename, 'r') as f:
            for l in f:
                self.ln += 1
                self.line = l.rstrip()
                self.s_line = self.line.strip()
                self.ntabs = self.getNumTabs()
                if self.ntabs < self.lastLineTabs:  # DEDENT
                    self.CheckDedent()
                if self.ntabs > self.lastLineTabs:
                    self.CheckIndent()
                self.lastLine = self.line
                self.lastSLine = self.s_line
                self.lastLineTabs = self.ntabs

BracketTracker(sys.argv[1])
BracketFormatter(sys.argv[1])
