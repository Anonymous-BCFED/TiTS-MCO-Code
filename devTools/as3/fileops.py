import codecs
import os
import re
import shutil
import time
import chardet
import ftfy

from buildtools import log

class IndentWriter(object):

    def __init__(self, fh, indent_level=0, indent_chars='\t', variables={}):
        self._f = fh
        self.indent_level = indent_level
        self.indent_chars = indent_chars
        self.variables = variables

    def writeline(self, string=''):
        if string == '':
            self._f.write('\n')
        else:
            self._f.write('{}{}\n'.format((self.indent_chars * self.indent_level), self.format(string)))
        return self

    def format(self, string):
        for key, value in self.variables.items():
            string = string.replace('{{{}}}'.format(key), str(value))
        return string

    def __enter__(self):
        self.indent_level += 1

    def __exit__(self, type, value, traceback):
        self.indent_level -= 1


def countIndents(line):
    i = 0
    for c in line:
        if c == ' ':
            i += 1
        elif c == '\t':
            i += 4
        else:
            return i
    return i


def getIndentChars(line):
    buf = ''
    for c in line:
        if c == ' ' or c == '\t':
            buf += c
        else:
            return buf
    return buf


def calculateNewImports(readImports, requiredImports):
    # print(repr(readImports))
    newImports = []
    for imp in requiredImports:
        chunks = imp.split('.')
        chunks[-1] = '*'
        genimport = '.'.join(chunks)
        if genimport in readImports or imp in readImports:
            continue
        else:
            newImports += [imp]
    return newImports


def ensureConditionalImports(filename, matchToImports, sort=False):
    requires = []
    encoding = detect_encoding(filename)
    with open(filename, 'r', encoding=encoding) as f:
        for line in f:
            line = line.strip()
            for match, imports in matchToImports.items():
                if re.search(match, line) is not None:
                    requires += [i for i in imports if i not in requires]
    if len(requires) > 0:
        ensureImportsExist(filename, requires, sort=sort)


def detect_encoding(filename, detect_only=False):
    toread = min(32, os.path.getsize(filename))
    raw = b''
    with open(filename, 'rb') as f:
        raw = f.read(toread)
    encoding = 'utf-8-sig'
    if raw.startswith(codecs.BOM_UTF8):
        encoding = 'utf-8-sig'
    else:
        result = chardet.detect(raw)
        encoding = result['encoding']
        if encoding in ('utf-8', 'ascii'):
            encoding = 'utf-8-sig'
        if encoding in ('cp1252', 'Windows-1252'):
            encoding = 'cp1252'
    #log.info('chardet guesses: {}'.format(encoding))
    if encoding in ('utf-8-sig', 'cp1252') and not detect_only:
        with open(filename, 'r', encoding=encoding) as inf:
            with open(filename + '.utf8', 'w', encoding='utf-8-sig') as outf:
                for line in ftfy.fix_file(inf, fix_entities=False, fix_latin_ligatures=False, fix_character_width=False, uncurl_quotes=False):
                    outf.write(line)
        attempt=0
        while attempt<5:
            attempt += 1
            try:
                if os.path.isfile(filename):
                    os.remove(filename)
                if os.path.isfile(filename):
                    log.error('FAILED TO DELETE %s!', filename)
                    continue
                break
            except PermissionError:
                log.error("[%d/%d] Failed to delete %s, trying again in 1s.", attempt, 5, filename)
                time.sleep(1)
        shutil.move(filename + '.utf8', filename)
    return encoding


def ensureImportsExist(filename, requiredImports, sort=False):
    REG_IMPORT_STOP_A = re.compile(r'(public|private) (class|function)')
    REG_IMPORT_STOP_B = re.compile(r'/\*\*')
    # import classes.GameData.PerkClasses.*;
    #import classes.StorageClass;
    REG_IMPORT = re.compile(r'import ([a-zA-Z0-9_\.\*]+);')

    def matches(line, regex, action=None):
        m = regex.search(line)
        if m is not None:
            if action is not None:
                action(line, m)
            return True
        return False
    readImports = []
    encoding = detect_encoding(filename)
    with codecs.open(filename, 'r', encoding=encoding) as f:
        with codecs.open(filename + '.tmp', 'w', encoding='utf-8-sig') as w:
            ln = 0
            lastIndent = ''
            writingImports = True
            for line in f:
                ln += 1
                oline = line
                currentLine = line.lstrip().strip('\r\n')
                indent = getIndentChars(oline)
                line = line.strip()
                if writingImports:
                    m = REG_IMPORT.search(line)
                    if m is not None:
                        readImports += [m.group(1)]
                        lastIndent = indent
                        if sort:
                            continue
                    if matches(line, REG_IMPORT_STOP_A) or matches(line, REG_IMPORT_STOP_B):
                        added = calculateNewImports(readImports, requiredImports)
                        if sort:
                            added += readImports
                            added.sort()
                        if added:
                            for newImport in sorted(added):
                                w.write('{}import {};\n'.format(indent, newImport))
                            w.write('\n')
                        writingImports = False
                w.write(indent + currentLine + '\n')
    shutil.copy(filename + '.tmp', filename)
    os.remove(filename + '.tmp')


def GenIndentDeltas(lines, permitted_preprocs=['#if','#endif']):
    lastIndent = 0
    currentIndent = 0
    diff = 0
    lineInfo = []
    for ln in range(len(lines)):
        line = lines[ln]
        currentIndent = countIndents(line)
        if ln == 0:
            lastIndent = currentIndent
        if line.startswith('\b'):
            currentIndent = lastIndent
            line = line[1:]
        line = line.strip()
        if line.startswith('#'):
            if line.split(' ')[0] not in permitted_preprocs:
                lineInfo += [(None, 0, line)]
                continue
        diff = 0
        if currentIndent < lastIndent:
            diff = -1
        if currentIndent > lastIndent:
            diff = 1
        lineInfo += [(diff, currentIndent, line)]
        lastIndent = currentIndent
    return lineInfo


def writeGivenIndentData(writefunc, lines, writeIndentedLine, offsets=0, override_writeindented=None):
    indent = 0
    indentOffsets = ['manuallyoffset'] * offsets

    def innerwriteindented(curindent, line):
        writefunc(' ' * (curindent * 4) + line + '\n')

    def writeindented(line, indentOffset, offset=0):
        innerwriteindented((indent + indentOffset), line)
    if override_writeindented:
        innerwriteindented = override_writeindented
    nLines = len(lines)
    lastLineIndented = False
    print(nLines)
    for i in range(nLines):
        diff, currentIndent, line = lines[i]
        ndiff = None
        ndiffidx = 1
        while ndiff is None:
            if i + ndiffidx < nLines:
                ndiff, _, _ = lines[i + ndiffidx]
                ndiffidx += 1
            else:
                break
        if diff is None:
            line = line.strip()
            if line.startswith('#comment'):
                continue
            if line.startswith('#startblock'):
                statement = line[12:].strip()
                if ndiff == 1:
                    indentOffsets.append(statement)
                writeindented(statement, len(indentOffsets))
                if ndiff < 1:
                    indentOffsets.append(statement)
                continue
            if line == '#endblock':
                statement = indentOffsets.pop()
                writeindented('# END {}'.format(statement), len(indentOffsets))
                continue
            print('UNKNOWN TEMPLATE ENGINE COMMAND: ' + line.strip())
            continue
        indent = max(indent + diff, 0)
        lastLineIndented = writeIndentedLine(indent, diff, ndiff, line, len(indentOffsets), writeindented)


def escapePython(line):
    return line.replace(u"'", u"\\'").replace('\\', '\\\\')


def _writePythonIndentWriterLine(indent, diff, ndiff, line, indentOffset, writeindented):
    if ndiff > 0:  # If next line indents:
        writeindented('with w.writeline(\'{}\'):'.format(escapePython(line)), indentOffset, offset=-1)
        # writeindented(line)
        #indent += 1
        return True
    else:
        if line == '':
            writeindented('w.writeline()', indentOffset)
        else:
            writeindented('w.writeline(\'{}\')'.format(escapePython(line)), indentOffset)
        # writeindented(line)
        return False


def _writeBasicCorrectedIndent(indent, diff, ndiff, line, indentOffset, writeindented):
    # linedbg=/*{} {}*/'.format(diff, ndiff)
    # writeindented(linedbg+line,indentOffset)
    writeindented(line, indentOffset)
    if ndiff is None:
        ndiff = 0
    return ndiff > 0


def writeIndentWriterTemplate(writefunc, lines, offset=0):
    writeGivenIndentData(writefunc, GenIndentDeltas(lines), _writePythonIndentWriterLine, offset)


def writeReindentedViaIndentWriter(w, lines, offset=0):
    oldil = w.indent_level

    def _mywriteindented(indent, line):
        w.indent_level = indent
        w.writeline(line)
    writeGivenIndentData(None, GenIndentDeltas(lines), _writeBasicCorrectedIndent, offset, override_writeindented=_mywriteindented)
    w.indent_level = oldil


def writeReindented(writefunc, lines, offset=0):
    writeGivenIndentData(writefunc, GenIndentDeltas(lines), _writeBasicCorrectedIndent, offset)


def test_deltas():
    TestData = [
        '0',
        '0',
        '    1',
        '    0',
        '-1'
    ]
    i = 0
    for diff, currentIndent, line in GenIndentDeltas(TestData):
        i += 1
        realdiff = int(line.strip())
        if realdiff != diff:
            print(i, '{} != {}'.format(realdiff, diff))
