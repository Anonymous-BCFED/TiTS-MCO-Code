# Manual Merge Checklist
These files must be manually merged.

* [ ] classes\Characters\PlayerCharacter.as:
	* [ ] 1389 - `@BEGINPERKFUNC Nuki Nuts.OnPerkRacialUpdateCheck(pc:PlayerCharacter, deltaT:uint, doOut:Boolean):void`
	* [ ] 1453 - `@BEGINPERKFUNC Fecund Figure.OnPerkRacialUpdateCheck(pc:PlayerCharacter, deltaT:uint, doOut:Boolean):void`
	* [ ] 1520 - `@BEGINPERKFUNC Slut Stamp.OnPerkRacialUpdateCheck(pc:PlayerCharacter, deltaT:uint, doOut:Boolean):void`
	* [ ] 1537 - `@BEGINPERKFUNC Androgyny.OnPerkRacialUpdateCheck(pc:PlayerCharacter, deltaT:uint, doOut:Boolean):void`
	* [ ] 1569 - `@BEGINPERKFUNC Black Latex.OnPerkRacialUpdateCheck(pc:PlayerCharacter, deltaT:uint, doOut:Boolean):void`
* [ ] classes\Creature.as:
	* [ ] 3890 - `@BEGINPERKFUNC Nuki Nuts.OnPenisOrgasm(pc:PlayerCharacter, msg:StringValueHolder, cumAmount:NumberValueHolder):void`
	* [ ] 8938 - `@BEGINPERKFUNC Big Cock.OnCockIncrease(c:Creature, increase:NumberValueHolder):void`
	* [ ] 8947 - `@BEGINPERKFUNC Phallic Potential.OnCockIncrease(c:Creature, increase:NumberValueHolder):void`
	* [ ] 8955 - `@BEGINPERKFUNC Phallic Restraint.OnCockIncrease(c:Creature, increase:NumberValueHolder):void`
	* [ ] 11033 - `@BEGINPERKFUNC Nuki Nuts.OnAfterCumQ(c:Creature, hQuantity:NumberValueHolder):void`
	* [ ] 11145 - `@BEGINPERKFUNC Nuki Nuts.OnFilled(c:Creature, m:StringValueHolder, cumCascade:Boolean):void`
	* [ ] 11180 - `@BEGINPERKFUNC Nuki Nuts.OnOverfilling(c:Creature, capFullness:BoolValueHolder):void`
	* [ ] 12231 - `@BEGINPERKFUNC Hung.BeforeSetNewCockValues(c:Creature, arg:int):void`
	* [ ] 20708 - `@BEGINPERKFUNC Hung.OnWeighClitoris(c:Creature, weightClitoris:NumberValueHolder):void`
	* [ ] 20767 - `@BEGINPERKFUNC Hung.OnWeighPenis(c:Creature, weightPenis:NumberValueHolder):void`
* [ ] classes\Items\Miscellaneous\NukiCookies.as:
	* [ ] 72 - `@BEGINPERKFUNC Nuki Syndrome.ThicknessTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 85 - `@BEGINPERKFUNC Nuki Syndrome.ToneTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 121 - `@BEGINPERKFUNC Nuki Syndrome.ButtTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 172 - `@BEGINPERKFUNC Nuki Syndrome.DrunkTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 187 - `@BEGINPERKFUNC Nuki Syndrome.HairColorTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 199 - `@BEGINPERKFUNC Nuki Syndrome.FurColorTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 211 - `@BEGINPERKFUNC Nuki Syndrome.EyeTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 230 - `@BEGINPERKFUNC Nuki Syndrome.HairTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 261 - `@BEGINPERKFUNC Nuki Syndrome.EarTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 279 - `@BEGINPERKFUNC Nuki Syndrome.TailTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 363 - `@BEGINPERKFUNC Nuki Syndrome.NukiFaceTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 381 - `@BEGINPERKFUNC Nuki Syndrome.MaskFaceTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 399 - `@BEGINPERKFUNC Nuki Syndrome.HandTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 418 - `@BEGINPERKFUNC Nuki Syndrome.FurTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 445 - `@BEGINPERKFUNC Nuki Syndrome.NukiNutsTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 467 - `@BEGINPERKFUNC Nuki Nuts.Activate(c:Creature):void`
	* [ ] 481 - `@BEGINPERKFUNC Nuki Syndrome.BallTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 559 - `@BEGINPERKFUNC Nuki Syndrome.CockTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 621 - `@BEGINPERKFUNC Nuki Syndrome.VagTF(pc:Creature, changes:int, changeLimit:int):int`
	* [ ] 699 - `@BEGINPERKFUNC Nuki Syndrome.LegTF(pc:Creature, changes:int, changeLimit:int):int`
