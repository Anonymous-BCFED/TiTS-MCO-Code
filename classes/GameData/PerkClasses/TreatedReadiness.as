﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\newTexas\treatment.as:1694 (_parsedPerkCreationFindOnly): pc.createPerk("Treated Readiness",0,0,0,0,"Increases minimum lust to 33, ensuring you’re always ready to go.");
 * - includes\newTexas\treatment.as:1939 (_parsedPerkCreationFindOnly): pc.createPerk("Treated Readiness",0,0,0,0,"Increases minimum lust to 33, ensuring you’re always ready to go.");
 * Checked:
 * - includes\newTexas\treatment.as:1691: if(!pc.hasPerk("Treated Readiness"))
 * - includes\newTexas\treatment.as:1936: if(!pc.hasPerk("Treated Readiness"))
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class TreatedReadiness extends PerkData {
		public function TreatedReadiness() {
			setID("Treated Readiness");
			perkName = "Treated Readiness";
			setAllNames(["Treated Readiness"]);
			perkDescription = "Increases minimum lust to 33, ensuring you’re always ready to go.";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:TreatedReadiness = new TreatedReadiness();
			return _clone(sc, pd);
		}

	}
}

