﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * Checked:
 * - includes\events\syriQuest\syriQuestMain.as:811: if (pc.hasPerk("Stealth Field Generator")) addButton(0,"Stealth Field",syriQuestStealthField,undefined,"Stealth Field","Turn on your Stealth Field Generator and sneak aboard.");
 * WARNING: 1 checks, but 0 invocations!  This perk may be obsolete or broken!
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class StealthFieldGenerator extends PerkData {
		public function StealthFieldGenerator() {
			setID("Stealth Field Generator");
			perkName = "Stealth Field Generator";
			setAllNames(["Stealth Field Generator"]);
			perkDescription = "";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:StealthFieldGenerator = new StealthFieldGenerator();
			return _clone(sc, pd);
		}

	}
}

