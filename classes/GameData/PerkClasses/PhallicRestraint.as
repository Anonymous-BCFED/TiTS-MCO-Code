﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * Checked:
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;
	import classes.Util.NumberValueHolder;

	public class PhallicRestraint extends PerkData {
		public function PhallicRestraint() {
			setID("Phallic Restraint");
			perkName = "Phallic Restraint";
			setAllNames(["Phallic Restraint"]);
			perkDescription = "";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			c.OnCockIncrease.Attach(this, OnCockIncrease);
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			c.OnCockIncrease.Detach(this, OnCockIncrease);
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:PhallicRestraint = new PhallicRestraint();
			return _clone(sc, pd);
		}

		// Automatically generated (definition at classes\Creature.as:8955)
		private function OnCockIncrease(c:Creature, increase:NumberValueHolder):void {
			if(DEBUG) trace(perkName+" - OnCockIncrease!")
			increase.value *= .25;
		}

	}
}

