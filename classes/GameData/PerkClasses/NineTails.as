﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * Checked:
 * - classes\Items\Miscellaneous\ImmunoBooster.as:84: if (target.hasPerk("Enlightened Nine-tails") || target.hasPerk("Nine-tails"))
 * - classes\Items\Transformatives\Foxfire.as:703: if (target.hasPerk("Enlightened Nine-tails") || target.hasPerk("Nine-tails"))
 * WARNING: 2 checks, but 0 invocations!  This perk may be obsolete or broken!
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class Ninetails extends PerkData {
		public function Ninetails() {
			setID("Ninetails");
			perkName = "Nine-tails";
			setAllNames(["Nine-tails"]);
			perkDescription = "";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:Ninetails = new Ninetails();
			return _clone(sc, pd);
		}

	}
}

