﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\events\bimboPennyAndBadgerQuest\badgerGifts.as:221 (_parsedPerkCreationFindOnly): if(!pc.hasPerk("Ditz Speech")) pc.createPerk("Ditz Speech",0,0,0,0,"Alters dialogue in certain scenes.");
 * - includes\events\pexigaQuest\pexigaQuest.as:2791 (_parsedPerkCreationFindOnly): if(!pc.hasPerk("Ditz Speech")) pc.createPerk("Ditz Speech",0,0,0,0,"Alters dialogue in certain scenes.");
 * - includes\follower\mitzi.as:2629 (_parsedPerkCreationFindOnly): pc.createPerk("Ditz Speech",0,0,0,0,"Alters dialog in certain scenes.");
 * - includes\newTexas\treatment.as:1645 (_parsedPerkCreationFindOnly): pc.createPerk("Ditz Speech",0,0,0,0,"Alters dialogue in certain scenes.");
 * - includes\newTexas\treatment.as:4274 (_parsedPerkCreationFindOnly): pc.createPerk("Ditz Speech",0,0,0,0,"Alters dialogue in certain scenes.");
 * - includes\tarkus\drBadger.as:545 (_parsedPerkCreationFindOnly): pc.createPerk("Ditz Speech",0,0,0,0,"Alters dialogue in certain scenes.");
 * - includes\tarkus\dumbfuckBonus.as:317 (_parsedPerkCreationFindOnly): pc.createPerk("Ditz Speech",0,0,0,0,"Alters dialogue in certain scenes.");
 * Checked:
 * - classes\GameData\GroundCombatContainer.as:3528: if(pc.hasPerk("Ditz Speech")) output(" and giggle, <i>“Is this, like, what you wanted to see?”</i> ");
 * - includes\masturbation.as:1732: if(pc.hasPerk("Ditz Speech") || pc.hasPerk("Brute Speech")) output(" brainlessly");
 * - includes\masturbation.as:1760: if(pc.hasPerk("Ditz Speech")) output(" Airheaded giggles mix with your moans, declaring your status as a climax-addled milk-slut for the universe to see.");
 * - includes\events\bimboPennyAndBadgerQuest\badgerGifts.as:221: if(!pc.hasPerk("Ditz Speech")) pc.createPerk("Ditz Speech",0,0,0,0,"Alters dialogue in certain scenes.");
 * - includes\events\pexigaQuest\follower.pexiga.as:5: if (pexiga.hasPerk("Ditz Speech") && yammiIsCrew()) return true;
 * - includes\events\pexigaQuest\follower.pexiga.as:10: if (pexiga.hasPerk("Ditz Speech")) return true;
 * - includes\events\pexigaQuest\follower.pexiga.as:22: if(pexiga.hasPerk("Ditz Speech")) sBust += "_BIMBO";
 * - includes\events\pexigaQuest\pexigaQuest.as:2791: if(!pc.hasPerk("Ditz Speech")) pc.createPerk("Ditz Speech",0,0,0,0,"Alters dialogue in certain scenes.");
 * - includes\follower\mitzi.as:2626: if(!pc.hasPerk("Ditz Speech"))
 * - includes\follower\mitziWIP.as:256: if(!pc.hasPerk("Weak Willed") && !pc.hasPerk("Ditz Speech"))
 * - includes\mhenga\julianSHaswell.as:122: if(pc.hasPerk("Ditz Speech") || pc.hasPerk("Brute Speech")) output("\n\n<i>“Like, what’s a zil?”</i> you ask.");
 * - includes\mhenga\julianSHaswell.as:174: if(pc.hasPerk("Ditz Speech") || pc.hasPerk("Brute Speech")) output("<i>“No way, Doc!”</i> you say with a shake of your head. <i>“I’m not gonna do something that " + pc.mf("lame","mean") + "!”</i>");
 * - includes\newTexas\treatment.as:1640: if(startHours < 44 && treatedHours >= 44 && !pc.hasPerk("Ditz Speech") && !pc.hasPerk("Brute Speech"))
 * - includes\newTexas\treatment.as:3312: if(startHours < 46 && treatedHours >= 46 && !pc.hasPerk("Brute Speech") && !pc.hasPerk("Ditz Speech"))
 * - includes\newTexas\treatment.as:4216: if(pc.hasPerk("Ditz Speech") && canRemoveDitzSpeech())
 * - includes\newTexas\treatment.as:4271: if(tics >= 40 && !pc.hasPerk("Ditz Speech"))
 * - includes\tarkus\colenso.as:517: if(pc.isMischievous() || pc.hasPerk("Ditz Speech") || pc.hasPerk("Brute Speech"))
 * - includes\tarkus\drBadger.as:53: if(flags["DR_BADGER_BIMBOED_PC"] == undefined && !pc.hasPerk("Ditz Speech")) addButton(5,"Be Hero",heyDocImAHero,undefined,"Be Hero","Volunteer that you’re a hero. After your first encounter with the Doctor, you’re fairly sure this is going to result in some heavy brain-drain.");
 * - includes\tarkus\drBadger.as:195: else if(flags["DR_BADGER_BIMBOED_PC"] == undefined && !pc.hasPerk("Ditz Speech"))
 * - includes\tarkus\drBadger.as:202: else if(flags["DR_BADGER_BIMBOED_PC"] == undefined && pc.hasPerk("Ditz Speech"))
 * - includes\tarkus\drBadger.as:542: if(!pc.hasPerk("Ditz Speech"))
 * - includes\tarkus\dumbfuckBonus.as:310: if(flags["DUMBFUCK_SNEEZES"] >= 20 && !pc.hasPerk("Ditz Speech") && !pc.hasPerk("Brute Speech"))
 * - includes\tarkus\shekka.as:521: if(pc.hasPerk("Ditz Speech") || pc.hasPerk("Brute Speech"))
 * - includes\tarkus\shekka.as:524: if(pc.hasPerk("Ditz Speech")) output("giggle");
 * - includes\tarkus\shekka.as:527: if(pc.hasPerk("Ditz Speech")) output("Like, don’t worry about any of that! You sound super smart, and you like, totally care about them! That’s what’s important and stuff!");
 * - includes\tarkus\shekka.as:651: if(pc.hasPerk("Ditz Speech")) output(", I think...");
 * - includes\tarkus\shekka.as:708: if(pc.hasPerk("Ditz Speech") || pc.hasPerk("Brute Speech"))
 * - includes\tarkus\shekka.as:711: if(pc.hasPerk("Ditz Speech")) output("n airheaded giggle");
 * - includes\tarkus\shekka.as:777: if(pc.hasPerk("Ditz Speech"))
 * - includes\tarkus\stellarTether.as:1832: if(pc.hasPerk("Ditz Speech")) output(" Of course, sucking cocks feels awesome. Why wouldn’t it?");
 * - includes\tavros\jade.as:691: if(pc.hasPerk("Ditz Speech"))
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class DitzSpeech extends PerkData {
		public function DitzSpeech() {
			setID("Ditz Speech");
			perkName = "Ditz Speech";
			setAllNames(["Ditz Speech"]);
			perkDescription = "Alters dialogue in certain scenes.";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:DitzSpeech = new DitzSpeech();
			return _clone(sc, pd);
		}

	}
}

