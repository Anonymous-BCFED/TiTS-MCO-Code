﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - classes\Items\Transformatives\Holstaria.as:650 (_parsedPerkCreationFindOnly): target.createPerk("Bubble Butt", 0, 0, 0, 0, "Your ass is always soft, regardless of tone.");
 * - classes\Items\Transformatives\ThiccNShake.as:578 (_parsedPerkCreationFindOnly): target.createPerk("Bubble Butt", 0, 0, 0, 0, "Your ass is always soft, regardless of tone.");
 * Checked:
 * - classes\Items\Miscellaneous\AssSlapPatch.as:126: if(target.buttRatingRaw >= 10 && !target.hasPerk("Buns of Steel") && !target.hasPerk("Bubble Butt"))
 * - classes\Items\Miscellaneous\AssSlapPatch.as:145: if(target.hasPerk("Bubble Butt") && rand(2) == 0)
 * - classes\Items\Transformatives\Holstaria.as:160: if(!target.hasPerk("Bubble Butt") && !target.hasPerk("Buns of Steel")) tfList.push(28);
 * - classes\Items\Transformatives\ThiccNShake.as:513: if(!target.hasPerk("Bubble Butt") && target.buttRatingRaw >= 10 && !target.hasPerk("Buns of Steel"))
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.Engine.Utility.rand;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class BubbleButt extends PerkData {
		public function BubbleButt() {
			setID("Bubble Butt");
			perkName = "Bubble Butt";
			setAllNames(["Bubble Butt"]);
			perkDescription = "Your ass is always soft, regardless of tone.";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:BubbleButt = new BubbleButt();
			return _clone(sc, pd);
		}

	}
}

