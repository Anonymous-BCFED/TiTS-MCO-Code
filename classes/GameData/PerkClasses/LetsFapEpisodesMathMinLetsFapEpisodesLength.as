﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 * Updated 12/17/18
 * Invoked:
 * - .\includes\events\atha_lets_fapper.as:285: addButton(0,"LatestEpisode",LETS_FAP_EPISODES[Math.min((LETS_FAP_EPISODES.length - 1),flags["LETS_FAP_LATEST"])],undefined,"Latest Episode","Watch the latest episode of Let’s Fap!");
 * Checked:
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class LetsFapEpisodesMathMinLetsFapEpisodesLength extends PerkData {
		public function LetsFapEpisodesMathMinLetsFapEpisodesLength() {
			perkName = "LETS_FAP_EPISODES[Math.min((LETS_FAP_EPISODES.length - 1)";
			perkDescription = "";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:LetsFapEpisodesMathMinLetsFapEpisodesLength = new LetsFapEpisodesMathMinLetsFapEpisodesLength();
			return _clone(sc, pd);
		}

	}
}

