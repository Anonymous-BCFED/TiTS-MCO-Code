﻿/* @NO AUTOGEN@
 *
 * Updated 04/14/19
 * Invoked:
 * - classes\Items\Transformatives\Goblinola.as:620 (_parsedPerkCreationFindOnly): target.createPerk("Fecund Figure", 1, 1, 0, 0, "Your broodmare body permanently changes you more into a fertility goddess while you are pregnant.");
 * - classes\Items\Transformatives\ThiccNShake.as:564 (_parsedPerkCreationFindOnly): target.createPerk("Fecund Figure", 1, 1, 0, 0, "Your broodmare body permanently changes you more into a fertility goddess while you are pregnant.");
 * Checked:
 * - classes\Items\Transformatives\Goblinola.as:420: if(target.hasVagina() && (target.race() == "gabilani" || (target.isPregnant() && rand(4) == 0)) && !target.hasPerk("Fecund Figure"))
 * - classes\Items\Transformatives\ThiccNShake.as:506: if(!target.hasPerk("Fecund Figure") && (target.isPregnant() || target.fertility() > 0))
 * - includes\game.as:1854: if(pc.hasPerk("Fecund Figure") && pc.isSore())
 */

package classes.GameData.PerkClasses {
	import classes.Characters.PlayerCharacter;
	import classes.Creature;
	import classes.Engine.Interfaces.AddLogEvent;
	import classes.Engine.Interfaces.ExtendLogEvent;
	import classes.Engine.Utility.rand;
	import classes.GameData.PerkData;
	import classes.StorageClass;
	import classes.Util.BoolValueHolder;
	import classes.Util.NumberValueHolder;

	public class FecundFigure extends PerkData {
		public const VAL_HIP:Number = 0;
		public const VAL_BUTT:Number = 1;
		public const VAL_BELLY:Number = 2;
		public const VAL_GROWTH:Number = 3;

		public function FecundFigure() {
			setID("Fecund Figure");
			perkName = "Fecund Figure";
			setAllNames(["Fecund Figure"]);
			perkDescription = "Your broodmare body permanently changes you more into a fertility goddess while you are pregnant.";
			setStorageValues(0, 0, 0, 0);
			setStorageName(0, "hip size bonus");
			setStorageName(1, "butt size bonus");
			setStorageName(2, "belly size bonus (maybe)");
			setStorageName(3, "growth gains (adds per day while pregnant or post-pregnancy)");
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			c.OnPerkRacialUpdateCheck.Attach(this, OnPerkRacialUpdateCheck);
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			c.OnPerkRacialUpdateCheck.Detach(this, OnPerkRacialUpdateCheck);
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:FecundFigure = new FecundFigure();
			return _clone(sc, pd);
		}

		// Automatically generated (definition at classes\Characters\PlayerCharacter.as:1453)
		private function OnPerkRacialUpdateCheck(pc:PlayerCharacter, deltaT:uint, doOut:Boolean):void {
			if(DEBUG) trace(perkName+" - OnPerkRacialUpdateCheck!")
			if(!pc.hasVagina())
			{
				AddLogEvent("No longer possessing a vagina, your body tingles", "passive", deltaT);
				if((getValue(pc, VAL_HIP) + getValue(pc, VAL_BUTT) + getValue(pc, VAL_BELLY)) > 0) ExtendLogEvent(", rapidly changing as you lose your fertility goddess-like build");
				ExtendLogEvent(".\n\n(<b>Perk Lost: Fecund Figure</b>)");
				removeFrom(pc);
			}
		}

	}
}

