﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - classes\Items\Transformatives\Lupinol.as:478 (_parsedPerkCreationFindOnly): target.createPerk("Alpha Scent", 0, 0, 0, 0, "Your body produces pheromones that give you a distinctively commanding scent.");
 * Checked:
 * - classes\Items\Transformatives\Lupinol.as:140: if(!target.hasPerk("Alpha Scent") && (tfList.length <= 0 || target.lupineScore() >= 5)) tfList.push(18);
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class AlphaScent extends PerkData {
		public function AlphaScent() {
			setID("Alpha Scent");
			perkName = "Alpha Scent";
			setAllNames(["Alpha Scent"]);
			perkDescription = "Your body produces pheromones that give you a distinctively commanding scent.";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:AlphaScent = new AlphaScent();
			return _clone(sc, pd);
		}

	}
}

