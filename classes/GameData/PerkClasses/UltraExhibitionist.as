﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\masturbation\exhibitionismPerk.as:63 (_parsedPerkCreationFindOnly): pc.createPerk("Ultra-Exhibitionist", 1, 0, 0, 0, "The pleasures of exhibitionism have been seared into your body, locking your exhibitionism at max value. Also boosts minimum lust and lust gain when in civilization.");
 * Checked:
 * - includes\game.as:3143: if(pc.hasPerk("Ultra-Exhibitionist")) exhibitionismLocationToggle();
 * - includes\masturbation.as:690: if(pc.hasPerk("Ultra-Exhibitionist"))
 * - includes\travelEvents\shizuya.as:1482: output("You hear very obvious grunting and moaning as you approach the door. It’s not the kind of guttural intensity that you’d hear from an intense fucking, so you assume she’s probably by herself. For a second you think about coming back later, but then you remember who Shizuya is, and figure she’d probably like it if you walked in on her masturbating" + (pc.hasPerk("Ultra-Exhibitionist") ? ", you know you would." : ".") + " You open up the door and find something a bit different than you were expecting.");
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class Ultraexhibitionist extends PerkData {
		public function Ultraexhibitionist() {
			setID("UltraExhibitionist");
			perkName = "Ultra-Exhibitionist";
			setAllNames(["Ultra-Exhibitionist"]);
			perkDescription = "The pleasures of exhibitionism have been seared into your body, locking your exhibitionism at max value. Also boosts minimum lust and lust gain when in civilization.";
			setStorageValues(0, 0, 0, 0);
			setStorageName(0, "Public location flag toggle; 0 is not public, 1 is public");
			setStorageName(1, "Days since last exhibitionism event.");
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:Ultraexhibitionist = new Ultraexhibitionist();
			return _clone(sc, pd);
		}

	}
}

