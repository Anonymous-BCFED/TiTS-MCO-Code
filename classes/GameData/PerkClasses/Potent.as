﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\creation.as:1208 (_parsedPerkCreationFindOnly): pc.createPerk("Potent",1,0,0,0,"Increases the size of your orgasms and the speed at which you produce ejaculate.");
 * Checked:
 * - classes\Items\Transformatives\Equilicum.as:199: if (target.hasPerk("Potent") && maxCumModifierInLiters > 0) maxCumModifierInLiters *= 1.5;
 * - classes\Items\Transformatives\Equilicum.as:211: if (target.hasPerk("Potent") && maxCumModifierInLiters < 0) maxCumModifierInLiters *= 0.5;
 * - includes\creation.as:1120: if(pc.hasPerk("Potent"))
 * - includes\vesperia\ushamee.as:590: else output("You’ve long since changed yourself into the perfect form for just this occasion, ready and eager to thrust yourself deep inside this leithan beauty to spear her womb and flood it with your " + (pc.hasPerk("Potent") ? "potent " : "") + "seed. ");
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class Potent extends PerkData {
		public function Potent() {
			setID("Potent");
			perkName = "Potent";
			setAllNames(["Potent"]);
			perkDescription = "Increases the size of your orgasms and the speed at which you produce ejaculate.";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:Potent = new Potent();
			return _clone(sc, pd);
		}

	}
}

