﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 * Updated 12/17/18
 * Invoked:
 * - .\classes\Creature.as:17371: collection.push("salty", "salty", "salty", "salty", "salty", "salty", "salty", "potent", "potent", "potent");
 * - .\classes\Creature.as:17413: collection.push("salty", "salty", "salty", "salty", "salty", "salty", "salty", "potent", "potent", "potent");
 * Checked:
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class Salty extends PerkData {
		public function Salty() {
			perkName = "salty";
			perkDescription = "";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:Salty = new Salty();
			return _clone(sc, pd);
		}

	}
}

