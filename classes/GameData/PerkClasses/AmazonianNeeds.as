﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\newTexas\treatment.as:314 (_parsedPerkCreationFindOnly): pc.createPerk("Amazonian Needs",20,0,0,0,"Increases minimum lust by 20.");
 * Checked:
 * - includes\newTexas\treatment.as:311: if(!pc.hasPerk("Amazonian Needs"))
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class AmazonianNeeds extends PerkData {
		public function AmazonianNeeds() {
			setID("Amazonian Needs");
			perkName = "Amazonian Needs";
			setAllNames(["Amazonian Needs"]);
			perkDescription = "Increases minimum lust by 20.";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:AmazonianNeeds = new AmazonianNeeds();
			return _clone(sc, pd);
		}

	}
}

