﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\tavros\akane.as:122 (_parsedPerkCreationFindOnly): pc.createPerk("Akane's Painslut", 0, 0, 0, 0, "Your body is attuned to Akane’s methods of pleasurable pain. You have a chance to gain bonus Defense every time you take HP damage which increases with lower health... but you’ll also take additional lust damage. With higher lust, you gain bonus defense!");
 * Checked:
 * - classes\Engine\Combat\calculateHPDamage.as:50: if (target.hasPerk("Akane's Painslut")) defReduction += Math.round(((target.HPMax() - target.HP()) / target.HPMax()) * 5 * target.level);
 * - classes\Engine\Combat\calculateLustDamage.as:105: if (target.hasPerk("Akane's Painslut")) lustDef -= Math.round(((target.HPMax() - target.HP()) / target.HPMax()) * 5 * target.level);
 * - includes\tavros\akane.as:76: return pc.hasPerk("Akane's Painslut");
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class AkanesPainslut extends PerkData {
		public function AkanesPainslut() {
			setID("Akanes Painslut");
			perkName = "Akane's Painslut";
			setAllNames(["Akane's Painslut"]);
			perkDescription = "Your body is attuned to Akane’s methods of pleasurable pain. You have a chance to gain bonus Defense every time you take HP damage which increases with lower health... but you’ll also take additional lust damage. With higher lust, you gain bonus defense!";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:AkanesPainslut = new AkanesPainslut();
			return _clone(sc, pd);
		}

	}
}

