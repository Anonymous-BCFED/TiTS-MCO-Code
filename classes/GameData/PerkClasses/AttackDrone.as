﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * Checked:
 * - classes\Characters\CyberPunkSecOp.as:293: else if(target.hasPerk("Attack Drone") && target.shields() > 0 && !target.hasStatusEffect("Porno Hacked Drone") && !target.accessory.hasFlag(GLOBAL.ITEM_FLAG_COMBAT_DRONE) && rand(3) == 0 && this.energy() >= 20) pornoDroneHack(target);
 * - classes\Characters\MilodanWarAlpha.as:314: if(target.hasPerk("Attack Drone") && target.shields() > 0 && !target.hasStatusEffect("Porno Hacked Drone") && !target.accessory.hasFlag(GLOBAL.ITEM_FLAG_COMBAT_DRONE) && this.energy() >= 10) attackChoices.push(droneHackGo);
 * - classes\Characters\SexBot.as:291: if(target.hasPerk("Attack Drone") && target.shields() > 0 && !target.hasStatusEffect("Porno Hacked Drone") && !target.accessory.hasFlag(GLOBAL.ITEM_FLAG_COMBAT_DRONE)) choices[choices.length] = getDroneHacked;
 * - includes\items.tooltips.as:115: if(pc.hasPerk("Attack Drone")) tooltip += "\n\n<b>The droid replaces your original attack drone, giving you a battle companion at the expense of losing your drone’s bonus to shields.</b>";
 * - includes\items.tooltips.as:122: if(pc.hasPerk("Attack Drone")) tooltip += " <b>He replaces your original attack drone, giving you a powerful, bitey friend in battle at the expense of losing your drone’s bonus to shields.</b>";
 * - includes\events\karaquest2\roomFunctions.as:141: if (pc.hasPerk("Attack Drone")) output(" They look almost exactly like the drone you put together just after your Dad died, still tucked away in your pack.");
 * - includes\tarkus\stellarTether.as:396: if(pc.hasPerk("Attack Drone"))
 * WARNING: 7 checks, but 0 invocations!  This perk may be obsolete or broken!
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.Engine.Utility.rand;
	import classes.GLOBAL;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class AttackDrone extends PerkData {
		public function AttackDrone() {
			setID("Attack Drone");
			perkName = "Attack Drone";
			setAllNames(["Attack Drone"]);
			perkDescription = "";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:AttackDrone = new AttackDrone();
			return _clone(sc, pd);
		}

	}
}

