﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * Checked:
 * - classes\GameData\CombatAttacks.as:672: if(attacker.hasPerk("Cloak and Dagger"))
 * - classes\GameData\CombatAttacks.as:819: if(attacker.hasPerk("Cloak and Dagger"))
 * WARNING: 2 checks, but 0 invocations!  This perk may be obsolete or broken!
 */

package classes.GameData.PerkClasses {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class CloakAndDagger extends PerkData {
		public function CloakAndDagger() {
			setID("Cloak and Dagger");
			perkName = "Cloak and Dagger";
			setAllNames(["Cloak and Dagger"]);
			perkDescription = "";
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:CloakAndDagger = new CloakAndDagger();
			return _clone(sc, pd);
		}

	}
}

