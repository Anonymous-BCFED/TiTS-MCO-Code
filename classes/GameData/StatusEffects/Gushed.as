﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - classes\Characters\NymFoe.as:382 (_parsedStatusEffectCreationFindOnly): if(!target.hasStatusEffect("GUSHed")) target.createStatusEffect("GUSHed", 0, 0, 0, 0, false, "Icon_Poison", "You’re splattered in arousing drugs. Better not get hit by too many gushers, or you’ll be this robot’s simpering plaything.", true, 0);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class Gushed extends StatusData {
		public function Gushed() {
			setID("GUSHed");
			statusName = "GUSHed";
			setAllNames(["GUSHed"]);
			tooltip = ""You’re splattered in arousing drugs. Better not get hit by too many gushers, or you’ll be this robot’s simpering plaything."";
			iconName = ""Icon_Poison"";
			iconShade = 0xFFFFFF;
			hidden = false;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:Gushed = new Gushed();
			return _clone(sc, pd);
		}

	}
}

