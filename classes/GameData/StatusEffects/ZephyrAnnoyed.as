﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\newTexas\zephyr.as:972 (_parsedStatusEffectCreationFindOnly): if(!pc.hasStatusEffect("Zephyr Annoyed")) pc.createStatusEffect("Zephyr Annoyed",0,0,0,0,true,"","",false,120);
 * - includes\newTexas\zephyr.as:1103 (_parsedStatusEffectCreationFindOnly): if(!pc.hasStatusEffect("Zephyr Annoyed")) pc.createStatusEffect("Zephyr Annoyed",0,0,0,0,true,"","",false,1440);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class ZephyrAnnoyed extends StatusData {
		public function ZephyrAnnoyed() {
			setID("Zephyr Annoyed");
			statusName = "Zephyr Annoyed";
			setAllNames(["Zephyr Annoyed"]);
			tooltip = """";
			iconName = """";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:ZephyrAnnoyed = new ZephyrAnnoyed();
			return _clone(sc, pd);
		}

	}
}

