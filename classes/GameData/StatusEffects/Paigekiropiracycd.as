﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\events\kiro_x_paige_threesomes.as:423 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("PAIGE_KIRO_PIRACY_CD");
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class Paigekiropiracycd extends StatusData {
		public function Paigekiropiracycd() {
			setID("PAIGEKIROPIRACYCD");
			statusName = "PAIGE_KIRO_PIRACY_CD";
			setAllNames(["PAIGE_KIRO_PIRACY_CD"]);
			tooltip = "";
			iconName = "";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:Paigekiropiracycd = new Paigekiropiracycd();
			return _clone(sc, pd);
		}

	}
}

