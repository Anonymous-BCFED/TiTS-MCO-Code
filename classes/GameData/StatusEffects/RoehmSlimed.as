﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\breedwell\breedwell.as:965 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Roehm Slimed", 10, -5, 15, 0, false, "Icon_Splatter", "Direct physical contact with a roehm in heat results in stickiness, and smelling rather like an orgy in a vat of molasses.", false, 0, 0xB793C4);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class RoehmSlimed extends StatusData {
		public function RoehmSlimed() {
			setID("Roehm Slimed");
			statusName = "Roehm Slimed";
			setAllNames(["Roehm Slimed"]);
			tooltip = ""Direct physical contact with a roehm in heat results in stickiness, and smelling rather like an orgy in a vat of molasses."";
			iconName = ""Icon_Splatter"";
			iconShade = 0xB793C4;
			hidden = false;
			setStorageValues(10, -5, 15, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:RoehmSlimed = new RoehmSlimed();
			return _clone(sc, pd);
		}

	}
}

