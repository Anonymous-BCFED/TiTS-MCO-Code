﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\tavros\seraXPack2.as:307 (_parsedStatusEffectCreationFindOnly): if(!chars["SERA"].hasStatusEffect("Tickled Pink")) chars["SERA"].createStatusEffect("Tickled Pink");
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class TickledPink extends StatusData {
		public function TickledPink() {
			setID("Tickled Pink");
			statusName = "Tickled Pink";
			setAllNames(["Tickled Pink"]);
			tooltip = "";
			iconName = "";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:TickledPink = new TickledPink();
			return _clone(sc, pd);
		}

	}
}

