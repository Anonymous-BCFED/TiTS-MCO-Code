﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\tavros\resDeck\semith.as:471 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Roshan Blue",0,0,0,0,false,"Icon_DizzyDrunk","You see everything so very clear now - if only you could focus a little better.",false,0);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class RoshanBlue extends StatusData {
		public function RoshanBlue() {
			setID("Roshan Blue");
			statusName = "Roshan Blue";
			setAllNames(["Roshan Blue"]);
			tooltip = ""You see everything so very clear now - if only you could focus a little better."";
			iconName = ""Icon_DizzyDrunk"";
			iconShade = 0xFFFFFF;
			hidden = false;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:RoshanBlue = new RoshanBlue();
			return _clone(sc, pd);
		}

	}
}

