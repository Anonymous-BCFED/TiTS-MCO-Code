﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\breedwell\breedwell.as:1518 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Breedwell Cockmilker Porno");
 * - includes\breedwell\breedwell.as:1535 (_parsedStatusEffectCreationFindOnly): if(!pc.hasStatusEffect("Breedwell Cockmilker Porno")) pc.createStatusEffect("Breedwell Cockmilker Porno");
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class BreedwellCockmilkerPorno extends StatusData {
		public function BreedwellCockmilkerPorno() {
			setID("Breedwell Cockmilker Porno");
			statusName = "Breedwell Cockmilker Porno";
			setAllNames(["Breedwell Cockmilker Porno"]);
			tooltip = "";
			iconName = "";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:BreedwellCockmilkerPorno = new BreedwellCockmilkerPorno();
			return _clone(sc, pd);
		}

	}
}

