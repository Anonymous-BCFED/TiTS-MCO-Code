﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\follower\penny.as:887 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("PENNY_WAKEUP_CUED");
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class Pennywakeupcued extends StatusData {
		public function Pennywakeupcued() {
			setID("PENNYWAKEUPCUED");
			statusName = "PENNY_WAKEUP_CUED";
			setAllNames(["PENNY_WAKEUP_CUED"]);
			tooltip = "";
			iconName = "";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:Pennywakeupcued = new Pennywakeupcued();
			return _clone(sc, pd);
		}

	}
}

