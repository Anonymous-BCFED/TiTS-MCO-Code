﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\tavros\fisianna.as:352 (_parsedStatusEffectCreationFindOnly): else pc.createStatusEffect("Fisi Breeding Cooldown", 0, 0, 0, 0, true, "", "", false, 60*12);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class FisiBreedingCooldown extends StatusData {
		public function FisiBreedingCooldown() {
			setID("Fisi Breeding Cooldown");
			statusName = "Fisi Breeding Cooldown";
			setAllNames(["Fisi Breeding Cooldown"]);
			tooltip = """";
			iconName = """";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:FisiBreedingCooldown = new FisiBreedingCooldown();
			return _clone(sc, pd);
		}

	}
}

