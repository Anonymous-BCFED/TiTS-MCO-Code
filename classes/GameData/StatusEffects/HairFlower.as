﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - classes\Items\Transformatives\Cerespirin.as:310 (_parsedStatusEffectCreationFindOnly): target.createStatusEffect("Hair Flower", 1, 0, 0, 0, true, "", flowerColor, false);
 * - includes\holidayEvents\halloweenCostumes.as:4452 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Hair Flower", 1, 0, 0, 0, true, "", "red", false);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class HairFlower extends StatusData {
		public function HairFlower() {
			setID("Hair Flower");
			statusName = "Hair Flower";
			setAllNames(["Hair Flower"]);
			tooltip = "";
			iconName = """";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(1, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:HairFlower = new HairFlower();
			return _clone(sc, pd);
		}

	}
}

