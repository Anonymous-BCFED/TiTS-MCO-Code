﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\tarkus\grayGooArmor.as:660 (_parsedStatusEffectCreationFindOnly): attacker.createStatusEffect("Reduced Goo", defenseDown, 0, 0, 0, false, "Icon_DefDown", chars["GOO"].short + " has split from your frame and is busy teasing your foes - but it’s reduced your defense!", true, 0);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class ReducedGoo extends StatusData {
		public function ReducedGoo() {
			setID("Reduced Goo");
			statusName = "Reduced Goo";
			setAllNames(["Reduced Goo"]);
			tooltip = "";
			iconName = ""Icon_DefDown"";
			iconShade = 0;
			hidden = false;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:ReducedGoo = new ReducedGoo();
			return _clone(sc, pd);
		}

	}
}

