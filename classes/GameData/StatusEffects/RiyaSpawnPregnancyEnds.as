﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - classes\GameData\Pregnancy\Handlers\RiyaPregnancyHandler.as:208 (_parsedStatusEffectCreationFindOnly): mother.createStatusEffect("Riya Spawn Pregnancy Ends", babies, belly, pregSlot, babyGender, true);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class RiyaSpawnPregnancyEnds extends StatusData {
		public function RiyaSpawnPregnancyEnds() {
			setID("Riya Spawn Pregnancy Ends");
			statusName = "Riya Spawn Pregnancy Ends";
			setAllNames(["Riya Spawn Pregnancy Ends"]);
			tooltip = "";
			iconName = "";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:RiyaSpawnPregnancyEnds = new RiyaSpawnPregnancyEnds();
			return _clone(sc, pd);
		}

	}
}

