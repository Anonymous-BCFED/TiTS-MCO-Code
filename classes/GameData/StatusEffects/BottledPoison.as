﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - classes\Characters\NaleenHerm.as:303 (_parsedStatusEffectCreationFindOnly): target.createStatusEffect("Bottled Poison",4,0,0,0,false,"Icon_Poison","You’ve been struck by naleen poison, reducing physique, aim, and reflexes by 4!",false,4,0xFF0000);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class BottledPoison extends StatusData {
		public function BottledPoison() {
			setID("Bottled Poison");
			statusName = "Bottled Poison";
			setAllNames(["Bottled Poison"]);
			tooltip = ""You’ve been struck by naleen poison, reducing physique, aim, and reflexes by 4!"";
			iconName = ""Icon_Poison"";
			iconShade = 0xFF0000;
			hidden = false;
			setStorageValues(4, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:BottledPoison = new BottledPoison();
			return _clone(sc, pd);
		}

	}
}

