﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - classes\GameData\Pregnancy\Handlers\ZephyrPregnancyHandler.as:123 (_parsedStatusEffectCreationFindOnly): mother.createStatusEffect("Zephyr Preg Alert", 0, 0, 0, 0, true, "Icon_Belly_Pregnant", "", false, 60, 0xFFFFFF);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class ZephyrPregAlert extends StatusData {
		public function ZephyrPregAlert() {
			setID("Zephyr Preg Alert");
			statusName = "Zephyr Preg Alert";
			setAllNames(["Zephyr Preg Alert"]);
			tooltip = """";
			iconName = ""Icon_Belly_Pregnant"";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:ZephyrPregAlert = new ZephyrPregAlert();
			return _clone(sc, pd);
		}

	}
}

