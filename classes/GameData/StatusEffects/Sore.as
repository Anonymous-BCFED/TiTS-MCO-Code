﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\newTexas\tenTonGym.as:2380 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Sore", 0, 0, 0, 0, false, "Icon_Crying", "You are sore and will regain energy slower. Sleep to recover.", false, 0, 0xFFFFFF);
 * - includes\newTexas\tenTonGym.as:2395 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Sore", 0, 0, 0, 0, false, "Icon_Crying", "You are sore and will regain energy slower. Sleep to recover.", false, 0, 0xFFFFFF);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class Sore extends StatusData {
		public function Sore() {
			setID("Sore");
			statusName = "Sore";
			setAllNames(["Sore"]);
			tooltip = ""You are sore and will regain energy slower. Sleep to recover."";
			iconName = ""Icon_Crying"";
			iconShade = 0xFFFFFF;
			hidden = false;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:Sore = new Sore();
			return _clone(sc, pd);
		}

	}
}

