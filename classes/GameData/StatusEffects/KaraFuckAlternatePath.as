﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\events\icequeen\icequeen.as:1187 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Kara Fuck Alternate Path");
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class KaraFuckAlternatePath extends StatusData {
		public function KaraFuckAlternatePath() {
			setID("Kara Fuck Alternate Path");
			statusName = "Kara Fuck Alternate Path";
			setAllNames(["Kara Fuck Alternate Path"]);
			tooltip = "";
			iconName = "";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:KaraFuckAlternatePath = new KaraFuckAlternatePath();
			return _clone(sc, pd);
		}

	}
}

