﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\newTexas\tenTonGym.as:981 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Gym Pass", 0, 0, 0, 0, false, "Icon_Haste", "You have a temporary gym pass to the Ten Ton Gym on New Texas.", false, 1440);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class GymPass extends StatusData {
		public function GymPass() {
			setID("Gym Pass");
			statusName = "Gym Pass";
			setAllNames(["Gym Pass"]);
			tooltip = ""You have a temporary gym pass to the Ten Ton Gym on New Texas."";
			iconName = ""Icon_Haste"";
			iconShade = 0xFFFFFF;
			hidden = false;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:GymPass = new GymPass();
			return _clone(sc, pd);
		}

	}
}

