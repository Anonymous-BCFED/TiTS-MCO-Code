﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\tavros\seraXPack2.as:591 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Sera Morning Sickness", 0, 0, 0, 0, true, "", "", false, 120);
 * - includes\tavros\seraXPack2.as:4873 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Sera Morning Sickness", 0, 0, 0, 0, true, "", "", false, 120);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class SeraMorningSickness extends StatusData {
		public function SeraMorningSickness() {
			setID("Sera Morning Sickness");
			statusName = "Sera Morning Sickness";
			setAllNames(["Sera Morning Sickness"]);
			tooltip = """";
			iconName = """";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:SeraMorningSickness = new SeraMorningSickness();
			return _clone(sc, pd);
		}

	}
}

