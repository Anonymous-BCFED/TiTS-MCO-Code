﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\uveto\ula.as:2091 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Afraid of Ula Pussy");
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class AfraidOfUlaPussy extends StatusData {
		public function AfraidOfUlaPussy() {
			setID("Afraid of Ula Pussy");
			statusName = "Afraid of Ula Pussy";
			setAllNames(["Afraid of Ula Pussy"]);
			tooltip = "";
			iconName = "";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:AfraidOfUlaPussy = new AfraidOfUlaPussy();
			return _clone(sc, pd);
		}

	}
}

