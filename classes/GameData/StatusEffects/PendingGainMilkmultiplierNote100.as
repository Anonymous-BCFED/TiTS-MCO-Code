﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\newTexas\treatment.as:2684 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Pending Gain MilkMultiplier Note: 100");
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class PendingGainMilkmultiplierNote100 extends StatusData {
		public function PendingGainMilkmultiplierNote100() {
			setID("Pending Gain MilkMultiplier Note 100");
			statusName = "Pending Gain MilkMultiplier Note: 100";
			setAllNames(["Pending Gain MilkMultiplier Note: 100"]);
			tooltip = "";
			iconName = "";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:PendingGainMilkmultiplierNote100 = new PendingGainMilkmultiplierNote100();
			return _clone(sc, pd);
		}

	}
}

