﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - classes\Items\Transformatives\Lucifier.as:65 (_parsedStatusEffectCreationFindOnly): target.createStatusEffect("Lucifier Candy", timerStamp, 1, 0, 0, true, "Pill", "The the Lucifier’s candy sweetness linger on your tongue....", false, timerStamp,0xB793C4);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class LucifierCandy extends StatusData {
		public function LucifierCandy() {
			setID("Lucifier Candy");
			statusName = "Lucifier Candy";
			setAllNames(["Lucifier Candy"]);
			tooltip = ""The the Lucifier’s candy sweetness linger on your tongue...."";
			iconName = ""Pill"";
			iconShade = 0xB793C4;
			hidden = true;
			setStorageValues(0, 1, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:LucifierCandy = new LucifierCandy();
			return _clone(sc, pd);
		}

	}
}

