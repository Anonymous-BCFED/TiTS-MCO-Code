﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\vesperia\jesse.as:183 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Jesse Bar Cooldown",0,0,0,0,true,"","",false,60*24);
 * - includes\vesperia\jesse.as:449 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Jesse Bar Cooldown",0,0,0,0,true,"","",false,60*24);
 * - includes\vesperia\jesse.as:563 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Jesse Bar Cooldown",0,0,0,0,true,"","",false,60*12);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class JesseBarCooldown extends StatusData {
		public function JesseBarCooldown() {
			setID("Jesse Bar Cooldown");
			statusName = "Jesse Bar Cooldown";
			setAllNames(["Jesse Bar Cooldown"]);
			tooltip = """";
			iconName = """";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:JesseBarCooldown = new JesseBarCooldown();
			return _clone(sc, pd);
		}

	}
}

