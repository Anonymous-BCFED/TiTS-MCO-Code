﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\dynamicGrowth.as:7 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Egregiously Endowed", 0, 0, 0, 0, false, "Icon_Poison", "Movement between rooms takes twice as long, and fleeing from combat is more difficult.", false, 0);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class EgregiouslyEndowed extends StatusData {
		public function EgregiouslyEndowed() {
			setID("Egregiously Endowed");
			statusName = "Egregiously Endowed";
			setAllNames(["Egregiously Endowed"]);
			tooltip = ""Movement between rooms takes twice as long, and fleeing from combat is more difficult."";
			iconName = ""Icon_Poison"";
			iconShade = 0xFFFFFF;
			hidden = false;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:EgregiouslyEndowed = new EgregiouslyEndowed();
			return _clone(sc, pd);
		}

	}
}

