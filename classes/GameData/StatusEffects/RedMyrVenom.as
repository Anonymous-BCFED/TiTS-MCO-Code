﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\myrellion\venomAddiction.as:61 (_parsedStatusEffectCreationFindOnly): if(!pc.hasStatusEffect("Red Myr Venom")) pc.createStatusEffect("Red Myr Venom",0,0,0,0,false,"Icon_LustUp","The red myr venom coursing through your veins is keeping your cheeks flushed with growing arousal. The longer it lasts, the more your mind gives over to its primal desires...",false,720,0xB793C4);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class RedMyrVenom extends StatusData {
		public function RedMyrVenom() {
			setID("Red Myr Venom");
			statusName = "Red Myr Venom";
			setAllNames(["Red Myr Venom"]);
			tooltip = ""The red myr venom coursing through your veins is keeping your cheeks flushed with growing arousal. The longer it lasts, the more your mind gives over to its primal desires..."";
			iconName = ""Icon_LustUp"";
			iconShade = 0xB793C4;
			hidden = false;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:RedMyrVenom = new RedMyrVenom();
			return _clone(sc, pd);
		}

	}
}

