﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\masturbation\sleepFapnea.as:244 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Sleep Fapnea Dream", 0, 0, 0, 0, true, "", "", false, 0);
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class SleepFapneaDream extends StatusData {
		public function SleepFapneaDream() {
			setID("Sleep Fapnea Dream");
			statusName = "Sleep Fapnea Dream";
			setAllNames(["Sleep Fapnea Dream"]);
			tooltip = """";
			iconName = """";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:SleepFapneaDream = new SleepFapneaDream();
			return _clone(sc, pd);
		}

	}
}

