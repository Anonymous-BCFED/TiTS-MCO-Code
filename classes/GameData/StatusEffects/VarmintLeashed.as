﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - includes\newTexas\varmint_wrangling.as:768 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Varmint Leashed");
 * - includes\newTexas\varmint_wrangling.as:826 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Varmint Leashed");
 * - includes\newTexas\varmint_wrangling.as:987 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Varmint Leashed");
 * - includes\uveto\natalie.as:407 (_parsedStatusEffectCreationFindOnly): pc.createStatusEffect("Varmint Leashed");
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class VarmintLeashed extends StatusData {
		public function VarmintLeashed() {
			setID("Varmint Leashed");
			statusName = "Varmint Leashed";
			setAllNames(["Varmint Leashed"]);
			tooltip = "";
			iconName = "";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:VarmintLeashed = new VarmintLeashed();
			return _clone(sc, pd);
		}

	}
}

