﻿/* AUTOGENERATED.  **INCLUDE @NO AUTOGEN@ AND DELETE THIS LINE TO DISABLE OVERWRITE.**
 *
 * Updated 05/21/19
 * Invoked:
 * - classes\Characters\SlyverenSlavebreaker.as:400 (_parsedStatusEffectCreationFindOnly): this.createStatusEffect("SLYVEREN_HAZ_UR_LOOT");
 * - classes\Characters\SlyverenSlavebreaker.as:414 (_parsedStatusEffectCreationFindOnly): this.createStatusEffect("SLYVEREN_HAZ_UR_LOOT");
 * Checked:
 */

package classes.GameData.StatusEffects {
	import classes.Creature;
	import classes.GameData.PerkData;
	import classes.StorageClass;

	public class Slyverenhazurloot extends StatusData {
		public function Slyverenhazurloot() {
			setID("SLYVERENHAZURLOOT");
			statusName = "SLYVEREN_HAZ_UR_LOOT";
			setAllNames(["SLYVEREN_HAZ_UR_LOOT"]);
			tooltip = "";
			iconName = "";
			iconShade = 0xFFFFFF;
			hidden = true;
			setStorageValues(0, 0, 0, 0);
		}

		override public function Attach(c:Creature):void {
			super.Attach(c);
			// Add event listeners here.
		}

		override public function Detach(c:Creature):void {
			super.Detach(c);
			// Remove event listeners here.
		}

		override public function Activate(c:Creature):void {
			super.Activate(c);
			// Called when added to a creature.
		}

		override public function Deactivate(c:Creature):void {
			super.Deactivate(c);
			// Called when removed from a creature.
		}

		override public function clone(sc:StorageClass):PerkData {
			var pd:Slyverenhazurloot = new Slyverenhazurloot();
			return _clone(sc, pd);
		}

	}
}

