﻿package classes.GameData
{
	import classes.Creature;
	import classes.kGAMECLASS;
	import classes.StorageClass;

	/**
	 * Represents a perk in TiTS.MCO.
	 * @author Anonymous-BCFED
	 * @author Gedan
	 */
	public class PerkData
	{
		// Static init some filler perk data objects for reasons
		{
			UNKNOWN	= new PerkData();
			UNKNOWN.perkName = "UNAVAILABLE";
		}

		public static var UNKNOWN:PerkData;

		public var DEBUG:Boolean = false;

		/**
		 * MCO: Because Fenoxo is an idiot and used raw strings everywhere,
		 * some perks have misspellings and typos.  Encodings also cause problems.
		 *
		 * Here is what we define the perk's actual identifier to be, for internal consistency.
		 */
		private var _id:String = "";
		protected function setID(value:String):void { _id = value; }
		public function get ID():String { return _id; }

		/**
		 * MCO: All the names referenced for this perk, misspelt or not.
		 */
		private var _allNames:Array = [];
		protected function setAllNames(value:Array):void { _allNames = value; }
		public function get allNames():Array {
			return _allNames.slice(); // Cloned instance.
		}

		// perkName == storageClass.storageName - this is how we're gonna tie the two halves together.
		private var _perkName:String = "";
		public function get perkName():String { return _perkName; }
		public function set perkName(v:String):void { _perkName = v; }

		// Full display name, in case we need to change the display without fucking up the back end.
		private var _perkShortName:String = "";
		public function get perkShortName():String
		{
			if (_perkShortName.length == 0)
			{
				return _perkName;
			}
			else
			{
				return _perkShortName;
			}
		}
		public function set perkShortName(v:String):void { _perkShortName = v; }

		// Full description text.
		private var _perkDescription:String = "";
		public function get perkDescription():String { return _perkDescription; }
		public function set perkDescription(v:String):void { _perkDescription = v; }

		private var _autoGained:Boolean = false;
		public function get autoGained():Boolean { return _autoGained; }
		public function set autoGained(v:Boolean):void { _autoGained = v; }

		// Class this perk is limited to
		private var _classLimited:int = -1;
		public function get classLimit():int { return _classLimited; }
		public function set classLimit(v:int):void { _classLimited = v; }
		public function get isClassLimited():Boolean
		{
			if (_classLimited == -1)
			{
				return false;
			}
			else
			{
				return true;
			}
		}


		// Is a level-tiered perk
		// Level/Tier this perk is limited to
		private var _levelLimit:int = -1;
		public function get levelLimit():int { return _levelLimit; }
		public function set levelLimit(v:int):void { _levelLimit = v; }
		public function get isLevelLimited():Boolean
		{
			if (_levelLimit == -1)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		// MCO: This is such a stupid system.
		// TODO: MCO - Convert from valueStorage's static quartet to serialize()/deserialize() for more flexibility.

		// Storage class value usage
		private var _valueStorageNames:Array = ["", "", "", ""];
		public function setStorageName(slotNumber:int, name:String):void
		{
			if (slotNumber >= 0 && slotNumber <= 3) _valueStorageNames[slotNumber] = name;
		}
		public function getStorageName(slotNumber:int):String
		{
			if (slotNumber >= 0 && slotNumber <= 3) return _valueStorageNames[slotNumber];
			else throw new Error("Array access out of bounds.");
		}

		private var _valueStorageValues:Array = [0, 0, 0, 0];
		public function setStorageValues(v1:* = null, v2:* = null, v3:* = null, v4:* = null):void
		{
			if (v1 != null) _valueStorageValues[0] = v1;
			if (v2 != null) _valueStorageValues[1] = v2;
			if (v3 != null) _valueStorageValues[2] = v3;
			if (v4 != null) _valueStorageValues[3] = v4;
		}
		public function getStorageValue(slotNumber:int):String
		{
			return _valueStorageValues[slotNumber];
		}

		// Limiter Function -- Limit access to the perk based on the boolean return of a functor
		private var _limitedAccess:Boolean = false;
		public function get isLimitedAccess():Boolean { return _limitedAccess; }

		private var _limitedAccessFunctor:Function = null;
		public function get isAccessable():Boolean
		{
			if (_limitedAccessFunctor != null )
			{
				return _limitedAccessFunctor();
			}
			else
			{
				return true;
			}
		}
		public function set accessFunction(v:Function):void
		{
			_limitedAccessFunctor = v;
			_limitedAccess = true;
		}

		// Application Function -- "Complex" addition functions so we can kinda automate some of this shit a little
		private var _applicationFunction:Function = null;
		public function applyTo(target:Creature, skipActivate:Boolean = false, v1:* = null, v2:* = 0, v3:* = 0, v4:* = 0):void
		{
			// MCO: setValues BEFORE calling Attach().
			setStorageValues(v1, v2, v3, v4);

			if(!skipActivate) {
				Activate(target);
			}

			Attach(target);

			target.createPerk(this._id, // MCO: Used to use perkName.
				_valueStorageValues[0],
				_valueStorageValues[1],
				_valueStorageValues[2],
				_valueStorageValues[3],
				_perkDescription);
		}

		public function removeFrom(target:Creature, skipDeactivate:Boolean=false):void {
			if(!skipDeactivate) {
				Deactivate(target);
			}

			Detach(target);
			target.removePerk(_id); // MCO: perkName -> _id
		}
		public function loadSaveObject(o:StorageClass):void {
			this.setStorageValues(o.value1, o.value2, o.value3, o.value4);
		}


		public function set applicationFunction(v:Function):void { _applicationFunction = v; }

		/**
		 * When we're added to a creature via gameplay.
		 * @param	c
		 */
		public function Activate(c:Creature):void {}

		/**
		 * When we're removed from a creature via gameplay.
		 * @param	c
		 */
		public function Deactivate(c:Creature):void {}

		/**
		 * When the creature is loaded from a save OR on first activation (gameplay).
		 * @param c Creature we're interested in.
		 */
		public function Attach(c:Creature):void {
			trace("Attaching " + this.perkName+" to " + c.nameDisplay());
		}

		/**
		 * When we're removed from the creature via gameplay or save stuff.
		 * @param	c
		 */
		public function Detach(c:Creature):void {
			trace("Detaching " + this.perkName+" from " + c.nameDisplay());
		}

		protected function getValue(c:Creature, idx:Number):Number {
			return c.getPerkValue(perkName, idx);
		}
		protected function setValue(c:Creature, idx:Number, value:Number):void {
			c.setPerkValue(perkName, idx, value);
		}

		public function clone(sc:StorageClass):PerkData {
			var pd:PerkData = new PerkData();
			pd.perkName = sc.storageName;
			pd.perkDescription = sc.tooltip;
			return _clone(sc, pd);
		}
		protected function _clone(sc:StorageClass, pd:PerkData):PerkData {
			pd.setStorageValues(sc.value1, sc.value2, sc.value3, sc.value4);
			return pd;
		}
		public function PerkData()
		{
			// Doop doop, nothing to do.
		}
		public function hasPerk(c:Creature):Boolean {
			return c.hasPerk(perkName);
		}
	}

}
